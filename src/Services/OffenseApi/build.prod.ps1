# Публикуем приложение
dotnet publish -c release -o ./bin/publish
# Строим докер образ
docker image build -t set_offenseapi .
# Делаем локальный тег
docker tag set_offenseapi docker.manfice.ru/set_offenseapi
# Отправляем образ в репозиторий
docker push docker.manfice.ru/set_offenseapi
