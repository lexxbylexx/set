using System;
using System.Collections.Generic;
using AutoMapper;
using DataAccess.Context;
using Domain.Helpers;
using Domain.Managers.Configuration;
using Domain.Managers.Configuration.OffenseApi;
using Domain.Pipelines;
using FluentValidation.AspNetCore;
using MediatR;
using MediatR.Pipeline;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Offense.Application;
using OffenseApi.extensions;
using Serilog;

namespace OffenseApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                .AddFluentValidation(configuration =>
                    configuration.RegisterValidatorsFromAssembly(typeof(OffensesApplication).Assembly));

            services.AddControllers()
                .AddNewtonsoftJson();

            services.AddOffenseCors();

            #region Database integration

            var connection = 
                ConfigurationManager<OffenseApiConfiguration>.GetConnectionString("maindb")
                ?? ConfigurationManager<OffenseApiConfiguration>.Configuration.ConnectionStrings.MainDb;

            services.AddDbContext<MainContext>(builder =>
            {
                builder.UseNpgsql(connection,
                    optionsBuilder =>
                    {
                        optionsBuilder.EnableRetryOnFailure(5, TimeSpan.FromSeconds(30), new List<string>());
                    });
            });

            #endregion

            #region MediatR

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPreProcessorBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPerformanceBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestAuthBehavior<,>));

            services.AddMediatR(typeof(OffensesApplication));

            #endregion

            #region AutoMapper

            services.AddAutoMapper(typeof(OffensesApplication));

            #endregion

            #region DI

            services.AddSingleton(x => Log.Logger);
            services.AddScoped<TempItemsDictionary>();

            #endregion
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSetCors();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
