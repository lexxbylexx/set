using System;
using System.IO;
using Domain.Managers.Configuration;
using Domain.Managers.Configuration.OffenseApi;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;

namespace OffenseApi
{
    public class Program
    {
        public static int Main(string[] args)
        {
            Log.Information("��������� ������ �����������");

            Log.Information("������������� ������������ �������");

            // �������������� ������������ �������
            ConfigurationManager<OffenseApiConfiguration>.Initialize();

            Log.Information("������������� �������");
            // ������������� �������
            Log.Logger = CreateSerilogLogger();

            try
            {

                Log.Information("������� ��� ����");
                var host = CreateWebHost(ConfigurationManager<OffenseApiConfiguration>.Root, args);

                Log.Information("��������� ����");
                host.Run();

                Log.Information("��������� ��������� ����������");
                return 0;
            }
            catch (Exception e)
            {
                Log.Fatal(e, $"������ ����������� ���� � ��������� ������");
                return 1;
            }
            finally
            {
                Log.Information("��������� ������������");
                Log.CloseAndFlush();
            }
        }

        public static IWebHost CreateWebHost(IConfiguration configuration, string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .CaptureStartupErrors(false)
                .UseStartup<Startup>()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseConfiguration(configuration)
                .UseSerilog()
                .Build();


        private static ILogger CreateSerilogLogger()
        {
            return new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.File(
                    path: $"{Directory.GetCurrentDirectory()}/log/OffenseApi-{DateTime.Today:dd-MM-yyyy}",
                    outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}"
                )
                .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}")
                .CreateLogger();
        }

    }
}
