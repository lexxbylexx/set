﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Offense.Application.CQRS.Charts;

namespace OffenseApi.Controllers
{
    public class OffenseDashboardController : BaseController
    {
        [HttpGet("ObjectOffenseChart")]
        public async Task<IActionResult> ObjectOffenseChart()
        {
            return Ok(await Mediator.Send(new OffenseOnObjectQuery()));
        }

        [HttpGet("OffenseByAuthors")]
        public async Task<IActionResult> OffenseByAuthorsChart()
        {
            return Ok(await Mediator.Send(new OffenseByAuthorsQuery()));
        }

        [HttpGet("OffenseByLinks")]
        public async Task<IActionResult> OffenseByLinkChart()
        {
            return Ok(await Mediator.Send(new OffenseByLinksQuery()));
        }

        [HttpGet("offenseByResponsiblePerson")]
        public async Task<IActionResult> OffenseByResponsiblePersonCart()
        {
            return Ok(await Mediator.Send(new OffenseByResponsiblePersonQuery()));
        }
    }
}