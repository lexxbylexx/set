﻿using System;
using System.Threading.Tasks;
using Domain.DTO.Offense;
using Domain.Responses;
using Microsoft.AspNetCore.Mvc;
using Offense.Application.CQRS.Offense;

namespace OffenseApi.Controllers
{
    public class OffensesController : BaseController
    {
        [HttpGet]
        [ProducesResponseType(typeof(ResponsePagedList<OffenseDto>), 200)]
        public async Task<IActionResult> Get([FromQuery] OffenseListQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(OffenseDto), 200)]
        public async Task<IActionResult> Get(Guid id)
        {
            return Ok(await Mediator.Send(new OffenseQuery { Id = id}));
        }
        
        [HttpPost]
        [ProducesResponseType(typeof(OffenseDto), 200)]
        public async Task<IActionResult> Post([FromBody] OffenseCreateCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut]
        [ProducesResponseType(typeof(OffenseDto), 200)]
        public async Task<IActionResult> Put([FromBody] OffenseUpdateCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(OffenseDto), 200)]
        public async Task<IActionResult> Delete(Guid id)
        {
            return Ok(await Mediator.Send(new OffenseDeleteCommand { Id = id }));
        }
    }
}