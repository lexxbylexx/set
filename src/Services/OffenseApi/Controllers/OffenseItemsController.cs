﻿using System;
using System.Threading.Tasks;
using Domain.DTO.Offense;
using Microsoft.AspNetCore.Mvc;
using Offense.Application.CQRS.OffenseItem;

namespace OffenseApi.Controllers
{
    [ApiController]
    public class OffenseItemsController : BaseController
    {
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(OffenseItemDto), 200)]
        public async Task<IActionResult> Get(Guid id)
        {
            return Ok(await Mediator.Send(new OffenseItemQuery{Id = id}));
        }

        [HttpPost]
        [ProducesResponseType(typeof(OffenseDto), 200)]
        public async Task<IActionResult> Post([FromBody] OffenseItemCreateCommand request)
        {
            return Ok(await Mediator.Send(request));
        }

        [HttpPut]
        [ProducesResponseType(typeof(OffenseDto), 200)]
        public async Task<IActionResult> Put([FromBody] OffenseItemUpdateCommand request)
        {
            return Ok(await Mediator.Send(request));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(OffenseItemDto), 200)]
        public async Task<IActionResult> Delete(Guid id)
        {
            return Ok(await Mediator.Send(new OffenseItemDeleteCommand {Id = id}));
        }
    }
}