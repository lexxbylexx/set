﻿using Domain.Managers.Configuration;
using Domain.Managers.Configuration.OffenseApi;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace OffenseApi.extensions
{
    public static class OffenseStartupExtensions
    {
        public static void AddOffenseCors(this IServiceCollection services)
        {
            var allowedHosts = ConfigurationManager<OffenseApiConfiguration>.Configuration.AllowedHosts;

            services.AddCors(options =>
            {
                options.AddPolicy("default", builder => builder
                    .SetIsOriginAllowedToAllowWildcardSubdomains()
                    .WithOrigins(
                            allowedHosts.Front
                        )
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials()
                );
            });
        }

        public static IApplicationBuilder UseSetCors(this IApplicationBuilder app)
        {
            app.UseCors("default");

            return app;
        }
    }
}