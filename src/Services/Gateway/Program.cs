using System;
using System.IO;
using Domain.Managers.Configuration;
using Domain.Managers.Configuration.Gateway;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;

namespace Gateway
{
    public class Program
    {
        public static int Main(string[] args)
        {
            try
            {
                Log.Information("��������� ������ ���������� ������ ������");
                Log.Information("������������� ������������ �������");
                // �������������� ������������ �������
                InitConfiguration();

                Log.Information("������������� �������");
                // ������������� �������
                Log.Logger = CreateSerilogLogger();

                Log.Information("������� ��� ����");
                var host = CreateWebHost(ConfigurationManager<GatewayConfiguration>.Root, args);

                Log.Information("��������� ����");
                host.Run();

                Log.Information("��������� ��������� ����������");
                return 0;
            }
            catch (Exception e)
            {
                Log.Fatal(e, $"������ ���������� ������ ������ ���� � ��������� ������");
                return 1;
            }
            finally
            {
                Log.Information("��������� ������������");
                Log.CloseAndFlush();
            }
        }

        public static IWebHost CreateWebHost(IConfiguration configuration, string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .CaptureStartupErrors(false)
                .UseStartup<Startup>()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseConfiguration(configuration)
                .UseSerilog()
                .Build();

        private static ILogger CreateSerilogLogger()
        {
            return new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.File(
                    path: $"{Directory.GetCurrentDirectory()}/log/Gateway-{DateTime.Today:dd-MM-yyyy}",
                    outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}"
                )
                .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}")
                .CreateLogger();
        }

        private static void InitConfiguration()
        {
            var currentEnvironment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{currentEnvironment}.json", optional: true, reloadOnChange: true)
                .AddJsonFile("ocelot.json", true, true)
                .AddJsonFile($"ocelot.{currentEnvironment}.json", true, true)
                .AddEnvironmentVariables();

            ConfigurationManager<GatewayConfiguration>.Initialize(builder.Build());
        }
    }
}
