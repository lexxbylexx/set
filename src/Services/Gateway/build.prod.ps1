# Публикуем приложение
dotnet publish -c release -o ./bin/publish
# Строим докер образ
docker image build -t set_gateway .
# Делаем локальный тег
docker tag set_gateway docker.manfice.ru/set_gateway
# Отправляем образ в репозиторий
docker push docker.manfice.ru/set_gateway
