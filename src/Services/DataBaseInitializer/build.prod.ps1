# Публикуем приложение
dotnet publish -c release -o ./bin/publish
# Строим докер образ
docker image build -t set_dbinitializer .
# Делаем локальный тег
docker tag set_dbinitializer docker.manfice.ru/set_dbinitializer
# Отправляем образ в репозиторий
docker push docker.manfice.ru/set_dbinitializer
