using System;
using System.Collections.Generic;
using System.Reflection;
using DataAccess.Context;
using DataAccess.Context.Initializers;
using Domain.Managers.Configuration;
using Domain.Managers.Configuration.DataBaseInitializer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;

namespace DataBaseInitializer
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = ConfigurationManager<DataBaseInitializerConfiguration>.GetConnectionString("maindb")
                ?? ConfigurationManager<DataBaseInitializerConfiguration>.Configuration.ConnectionStrings.MainDb;
            var migrationAssembly = typeof(MainContext).GetTypeInfo().Assembly.GetName().Name;
            Log.Write(LogEventLevel.Information, $"{connectionString}:{migrationAssembly}");

            // Add-Migration InitialMigration -c MainContext -v
            services.AddDbContext<MainContext>(builder =>
            {
                builder.UseNpgsql(connectionString, optionsBuilder =>
                {
                    optionsBuilder.MigrationsAssembly(migrationAssembly);
                    optionsBuilder.EnableRetryOnFailure(15, TimeSpan.FromSeconds(30), new List<string>());
                });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            InitDatabase(app);
        }

        public static void InitDatabase(IApplicationBuilder app)
        {
            using var scope = app.ApplicationServices.CreateScope();

            try
            {
                Log.Information("�������� �������� � ���������� ���� ������");
                MainDbInitializer.Seed(scope);
                Log.Information("���� ������ ���������������� �������");
            }
            catch (Exception e)
            {
                Log.Fatal(e, "� �������� ������������� ���� ������ ��������� ����������� ������");
            }
        }
    }
}
