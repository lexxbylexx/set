# Build solution
npm run build:prod
# Build image
docker image build -t set_spa .
# Tag image before pushing on server hub
docker tag set_spa docker.manfice.ru/set_spa
# Push image to repository
docker push docker.manfice.ru/set_spa
