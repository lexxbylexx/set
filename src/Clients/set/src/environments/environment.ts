import { IAppConfig } from 'src/app/core/interfaces/config';

export const environment: IAppConfig = {
  production: false,
  hosts: {
    gateway: 'http://localhost:44364',
  },
  clientInfo: {
    clientId: 'set',
    grantType: 'password',
  },
};
