import { IAppConfig } from 'src/app/core/interfaces/config';

export const environment: IAppConfig = {
  production: false,
  hosts: {
    gateway: 'https://set.manfice.ru',
  },
  clientInfo: {
    clientId: 'set',
    grantType: 'password',
  },
};
