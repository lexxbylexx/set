import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotFoundComponent } from './shared/components/not-found/not-found.component';
import { ForbiddenComponent } from './shared/components/forbidden/forbidden.component';
import { SignInComponent } from './shared/modules/sign-in/sign-in-component/sign-in.component';

import { AuthGuardGuard } from './shared/guards';


const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardGuard],
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
  },
  {
    path: 'sign-in',
    loadChildren: () => import('./shared/modules/sign-in/sign-in.module').then(m => m.SignInModule),
  },
  {
    path: 'access-denied',
    component: ForbiddenComponent,
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
