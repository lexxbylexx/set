import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StoreService } from '../services';

export class TokenHttpInterceptor implements HttpInterceptor {

  constructor(
    private readonly store: StoreService,
  ) {
  }

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.store.getToken();

    // Дополнительные параметры текущего запроса
    let requestOptions = {};

    if (token) {
      requestOptions = {
        setHeaders: {
          Authorization: `Bearer ${token.access_token}`,
        },
      };
    }

    const currentRequest = request.clone(requestOptions);

    return next.handle(currentRequest);
  }
}
