import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { TokenHttpInterceptor } from './httpToken.intercept';
import { StoreService } from '../services';

export const httpTokenInterceptorInitializer = {
  provide: HTTP_INTERCEPTORS,
  useClass: TokenHttpInterceptor,
  multi: true,
  deps: [StoreService]
};
