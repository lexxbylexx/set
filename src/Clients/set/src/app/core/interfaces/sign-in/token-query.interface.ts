export interface ITokenQuery {
  username: string;
  password: string;
  grant_type?: string;
  client_id?: string;
}
