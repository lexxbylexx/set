import { IHosts } from './host.interface';
import { IClientInfo } from './client-info.interface';

export interface IAppConfig {
  production: boolean;
  hosts: IHosts;
  clientInfo: IClientInfo;
}
