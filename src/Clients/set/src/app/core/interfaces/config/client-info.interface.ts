export interface IClientInfo {
  clientId: string;
  grantType: string;
}
