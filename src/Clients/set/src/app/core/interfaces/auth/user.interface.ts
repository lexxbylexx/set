/** Информация о пользователе системы */
export interface IUser {
  /** Идентификатор пользователя */
  sub: string;
  /** Идентификатор системы */
  companyId: string;
  /** Адрес электронной почты */
  email: string;
  /** Права пользователя */
  features: string | Array<string>;
  /** Имя пользователя */
  name: string;
  /** Роль пользователя */
  role: string;
}
