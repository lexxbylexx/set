import { NzConfig } from 'ng-zorro-antd';

export const ngZorroConfig: NzConfig = {
  message: { nzTop: 120},
};
