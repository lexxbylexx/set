import { environment } from 'src/environments/environment';

export class SignInUrls {
  public static baseUrl = `${environment.hosts.gateway}`;
  public static token = `${SignInUrls.baseUrl}/connect/token`;
  public static userInfo = `${SignInUrls.baseUrl}/connect/userinfo`;
}
