import { environment } from 'src/environments/environment';

export class OffenseUrls {
  public static baseUrl = environment.hosts.gateway;
  public static offenseList = `${OffenseUrls.baseUrl}/offenses`;
  public static offense = `${OffenseUrls.baseUrl}/offenses`;
}
