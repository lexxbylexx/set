import { environment } from 'src/environments/environment';

export class OffenseChartUrls {
  public static baseUrl = environment.hosts.gateway;
  public static offenseOnObject = `${OffenseChartUrls.baseUrl}/offenseDashboard/objectOffenseChart`;
  public static offenseByAuthors = `${OffenseChartUrls.baseUrl}/offenseDashboard/offenseByAuthors`;
  public static offenseByLinks = `${OffenseChartUrls.baseUrl}/offenseDashboard/offenseByLinks`;
  public static offenseByResponsiblePerson = `${OffenseChartUrls.baseUrl}/offenseDashboard/offenseByResponsiblePerson`;
}
