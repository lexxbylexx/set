import { environment } from 'src/environments/environment';

export class OffenseItemUrls {
  public static baseUrl = environment.hosts.gateway;
  public static offenseItemController = `${OffenseItemUrls.baseUrl}/offenseItems`;
}
