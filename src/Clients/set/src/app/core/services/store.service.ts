import { Injectable } from '@angular/core';
import { ITokenResponse } from '../interfaces';

@Injectable({providedIn: 'root'})
export class StoreService {

  private storage: Storage;

  constructor() {
    this.storage = window.localStorage;
  }

  public get(key: string): string {
    const item = this.storage.getItem(key);
    return !!item ? item : null;
  }

  public set(key: string, value: string): void {
    this.storage.setItem(key, value);
  }

  public clear(key: string): void {
    this.storage.removeItem(key);
  }

  public getToken(): ITokenResponse {
    const storeValue = this.storage.getItem('token');

    let token: ITokenResponse;

    try {
      token = JSON.parse(storeValue);
    } catch (error) {
      this.clearToken();
    }
    return token ? token : null;
  }

  public clearToken(): void {
    this.storage.removeItem('token');
  }
}
