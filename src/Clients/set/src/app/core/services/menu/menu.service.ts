import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({providedIn: 'root'})
export class SideMenuService {
  public isCollapsed: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
}
