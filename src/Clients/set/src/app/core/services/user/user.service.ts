import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { IUser } from '../../interfaces/auth';
import { StoreService } from '../store.service';

@Injectable({ providedIn: 'root' })
export class UserService {

  public currentUser$: BehaviorSubject<IUser> = new BehaviorSubject<IUser>(null);

  public isSignIn$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private readonly store: StoreService,
  ) {
  }

  public setCurrentUser(user: IUser): void {
    if (user == null) {
      throw new Error('Не указан пользователь.');
    }

    this.store.set('currentUser', JSON.stringify(user));
    this.currentUser$.next(user);
    this.isSignIn$.next(true);
  }

  public getCurrentUser(): Observable<IUser> {
    const user = JSON.parse(this.store.get('currentUser'));

    this.currentUser$.next(user);

    return of(user);
  }

  public removeCurrentUser(): void {
    this.store.clear('currentUser');
    this.store.clearToken();
    this.currentUser$.next(null);
    this.isSignIn$.next(false);
  }

  public checkUserSignIn(): Observable<boolean> {
    const token = this.store.getToken();

    return of(token !== null);
  }
}
