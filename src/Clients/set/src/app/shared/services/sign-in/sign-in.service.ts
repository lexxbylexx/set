import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StoreService, UserService } from 'src/app/core/services';
import { Observable, of } from 'rxjs';
import { tap, switchMap, map } from 'rxjs/operators';

import { SignInDto } from '../../dto';
import { ITokenQuery, ITokenResponse } from 'src/app/core/interfaces';
import { environment } from 'src/environments/environment';
import { SignInUrls } from 'src/app/core/urls/sign-in/sign-in.urls';

@Injectable({
  providedIn: 'root'
})
export class SignInService {

  constructor(
    private readonly store: StoreService,
    private readonly userService: UserService,
    private readonly http: HttpClient,
  ) { }

  public signIn(loginDto: SignInDto): Observable<any> {
    this.logout();

    const params = this.prepareSignInRequest(loginDto);

    const payload = Object.entries(params)
      .reduce((acc, entry) => {
        const pair = entry.join('=');
        acc.push(pair);

        return acc;
      }, [])
      .join('&');

    const headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});

    return this.logout()
      .pipe(
        switchMap(() => this.http.post<ITokenResponse>(SignInUrls.token, payload, { headers })),
        tap((value: ITokenResponse ) => this.store.set('token', JSON.stringify(value))),
        switchMap(() => {
          return this.http.get(`${SignInUrls.userInfo}`);
        }),
        map((user: any) => {
          this.userService.setCurrentUser(user);

          return user;
        })
      );
  }

  private prepareSignInRequest(dto: SignInDto): ITokenQuery {
    return {
      username: dto.email,
      password: dto.password,
      client_id: environment.clientInfo.clientId,
      grant_type: environment.clientInfo.grantType,
    };
  }

  public logout(): Observable<boolean> {
    this.userService.removeCurrentUser();

    return of(true);
  }
}
