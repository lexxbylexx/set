import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SignInService } from 'src/app/shared/services/sign-in';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'set-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  public signInForm: FormGroup;

  public errorString: string;

  constructor(
    private readonly fb: FormBuilder,
    private readonly signIn: SignInService,
    private readonly router: Router,
  ) { }

  private initForm(): void {
    this.signInForm = this.fb.group({
      email: [undefined, [Validators.email, Validators.required]],
      password: [undefined, [Validators.required, Validators.minLength(8)]],
    });
  }

  public submit(): void {
    this.errorString = '';

    if (this.signInForm.invalid) {
      this.showError();

      return;
    }

    this.signIn.signIn(this.signInForm.value)
      .subscribe(
        () => {
          this.router.navigate(['dashboard']);
        },
        (error: HttpErrorResponse) => {
          this.errorString = error.status.toString();
        },
      );
  }

  /** Проверяет поля формы на ошибки */
  private showError(): void {
    Object.keys(this.signInForm.controls)
      .forEach((key: string) => {
        const control = this.signInForm.get(key);

        if (control.invalid) {
          this.errorString = Object.keys(control.errors).length > 0
            ? Object.keys(control.errors)[0]
            : '';

          return;
        }
      });
  }

  public ngOnInit(): void {
    this.initForm();
  }
}
