import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'set-home-page-root',
  templateUrl: './home-page-root.component.html',
  styleUrls: ['./home-page-root.component.scss'],
})
export class HomePageRootComponent implements OnInit {

  constructor() { }

  public ngOnInit(): void {

  }

}
