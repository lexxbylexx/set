import { Component, OnInit } from '@angular/core';
import { SideMenuService } from 'src/app/core/services';

@Component({
  selector: 'set-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

  public isCollapsed$ = this.ms.isCollapsed;
  constructor(
    private readonly ms: SideMenuService,
  ) { }

  public ngOnInit(): void {
  }
}
