import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services';
import { IUser } from 'src/app/core/interfaces/auth';

@Component({
  selector: 'set-side-menu-user',
  templateUrl: './side-menu-user.component.html',
  styleUrls: ['./side-menu-user.component.scss']
})
export class SideMenuUserComponent implements OnInit {

  public user: IUser;

  constructor(
    private readonly us: UserService,
  ) { }

  public ngOnInit(): void {
    this.us.getCurrentUser()
      .subscribe((user: IUser) => this.user = user);
  }
}
