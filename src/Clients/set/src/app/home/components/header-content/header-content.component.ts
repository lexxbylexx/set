import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SignInService } from 'src/app/shared/services/sign-in';

@Component({
  selector: 'set-header-content',
  templateUrl: './header-content.component.html',
  styleUrls: ['./header-content.component.scss']
})
export class HeaderContentComponent implements OnInit {

  constructor(
    private readonly signInService: SignInService,
    private readonly router: Router,
  ) { }

  public logout(): void {
    this.signInService.logout()
      .subscribe(() => this.router.navigate(['sign-in']));
  }

  public ngOnInit(): void {
  }

}
