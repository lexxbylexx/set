import { Component, OnInit } from '@angular/core';

import { DashboardService } from '../../shared/services';

import { IOffenseByLink } from '../../shared/interfaces';

import { ChartType, ChartOptions } from 'chart.js';

@Component({
  selector: 'set-offense-by-links',
  templateUrl: './offense-by-links.component.html',
  styleUrls: [
    './offense-by-links.component.scss',
    '../chart-common.scss',
  ],
})
export class OffenseByLinksComponent implements OnInit {
  public barChartType: ChartType = 'pie';
  public chartData: IOffenseByLink;
  public barChartLegend: boolean = true;
  public barChartOptions: ChartOptions = {
    responsive: true,
  };

  constructor(
    private readonly ds: DashboardService,
  ) { }

  public ngOnInit() {
    this.ds.getOffenseByLinksData()
      .subscribe((chartData: IOffenseByLink) => this.chartData = chartData);
  }
}
