import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OffenseCountByAuthorsComponent } from './offense-count-by-authors.component';

describe('OffenceCountByAuthorsComponent', () => {
  let component: OffenseCountByAuthorsComponent;
  let fixture: ComponentFixture<OffenseCountByAuthorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OffenseCountByAuthorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffenseCountByAuthorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
