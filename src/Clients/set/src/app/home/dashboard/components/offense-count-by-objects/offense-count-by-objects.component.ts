import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseChartDirective } from 'ng2-charts';
import { ChartType, ChartOptions } from 'chart.js';
import { IOffenseOnObject } from '../../shared/interfaces';
import { DashboardService } from '../../shared/services';

@Component({
  selector: 'set-offense-count-by-objects',
  templateUrl: './offense-count-by-objects.component.html',
  styleUrls: [
    './offense-count-by-objects.component.scss',
    '../chart-common.scss',
  ],
})
export class OffenseCountByObjectsComponent implements OnInit {
  @ViewChild(BaseChartDirective, {static: true}) public chart: BaseChartDirective;
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public chartData: IOffenseOnObject;

  constructor(
    private readonly ds: DashboardService,
  ) { }

  public ngOnInit(): void {
    this.ds.getOffenseOnObjectData()
      .subscribe((response: IOffenseOnObject) => this.chartData = response);
  }

}
