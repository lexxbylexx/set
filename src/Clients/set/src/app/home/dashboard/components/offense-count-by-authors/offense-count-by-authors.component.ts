import { Component, OnInit } from '@angular/core';

// Charts
import { ChartType, ChartOptions } from 'chart.js';

// Services
import { DashboardService } from '../../shared/services';

// Interfaces
import { IOffenseByAuthors } from '../../shared/interfaces';

@Component({
  selector: 'set-offense-count-by-authors',
  templateUrl: './offense-count-by-authors.component.html',
  styleUrls: [
    './offense-count-by-authors.component.scss',
    '../chart-common.scss',
  ],
})
export class OffenseCountByAuthorsComponent implements OnInit {
  public barChartType: ChartType = 'bar';

  public barChartLegend = false;

  public barChartOptions: ChartOptions = {
    responsive: true,
  };

  public chartData: IOffenseByAuthors;

  constructor(
    private readonly ds: DashboardService,
  ) { }

  public ngOnInit(): void {
    this.ds.getOffensesByAuthorsData()
      .subscribe((response: IOffenseByAuthors) => this.chartData = response);
  }
}
