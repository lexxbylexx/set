import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../shared/services';
import { ChartType, ChartOptions } from 'chart.js';
import { IOffenseByResponsiblePerson } from '../../shared/interfaces';

@Component({
  selector: 'set-offense-by-responsible-person',
  templateUrl: './offense-by-responsible-person.component.html',
  styleUrls: [
    './offense-by-responsible-person.component.scss',
    '../chart-common.scss',
  ]
})
export class OffenseByResponsiblePersonComponent implements OnInit {
  public barChartType: ChartType = 'radar';
  public chartData: IOffenseByResponsiblePerson;
  public barChartLegend: boolean = false;
  public barChartOptions: ChartOptions = {
    responsive: true,
  };

  constructor(
    private readonly ds: DashboardService,
  ) { }

  public ngOnInit(): void {
    this.ds.getOffenseByResponsiblePersonData()
      .subscribe((chartData: IOffenseByResponsiblePerson) => this.chartData = chartData);
  }

}
