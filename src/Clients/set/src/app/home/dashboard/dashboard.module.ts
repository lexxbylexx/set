import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardRootComponent } from './components/dashboard-root/dashboard-root.component';
import { OffenseCountByObjectsComponent } from './components/offense-count-by-objects/offense-count-by-objects.component';

import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { DashboardDataService, DashboardService } from './shared/services';
import { OffenseCountByAuthorsComponent } from './components/offense-count-by-authors/offense-count-by-authors.component';
import { OffenseByLinksComponent } from './components/offense-by-links/offense-by-links.component';
import { OffenseByResponsiblePersonComponent } from './components/offense-by-responsible-person/offense-by-responsible-person.component';

@NgModule({
  declarations: [
    DashboardRootComponent,
    OffenseCountByObjectsComponent,
    OffenseCountByAuthorsComponent,
    OffenseByLinksComponent,
    OffenseByResponsiblePersonComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ChartsModule,
    NgxChartsModule,
  ],
  providers: [
    DashboardDataService,
    DashboardService,
  ],
})
export class DashboardModule { }
