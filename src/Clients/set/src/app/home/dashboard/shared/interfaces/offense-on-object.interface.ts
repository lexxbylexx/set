import { ChartDataSets } from 'chart.js';

export interface IOffenseOnObject {
  dataSet: Array<ChartDataSets>;
  dataLabels: Array<string>;
}
