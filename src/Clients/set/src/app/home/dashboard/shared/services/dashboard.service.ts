import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { DashboardDataService } from './dashboard-data.service';

import { IOffenseOnObject, IOffenseByAuthors, IOffenseByLink, IOffenseByResponsiblePerson } from '../interfaces';

import { BehaviorSubject, Observable, combineLatest, of } from 'rxjs';
import { tap, switchMap, catchError, filter } from 'rxjs/operators';

@Injectable()
export class DashboardService extends DashboardDataService {

  public update$: BehaviorSubject<boolean> = new BehaviorSubject(true);
  public error$: BehaviorSubject<any> = new BehaviorSubject(null);
  public loadData$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor (
    protected readonly http: HttpClient,
  ) {
    super(http);
  }

  public getOffenseOnObjectData(): Observable<IOffenseOnObject> {
    this.loadData$.next(true);

    const subscribers = combineLatest([
      this.update$
        .pipe(
          tap(() => this.error$.next(null)),
        ),
    ]);

    return subscribers
      .pipe(
        switchMap(() => {

          return this.getOffenseObjectDataset()
            .pipe(
              tap(() => this.loadData$.next(false)),
              catchError((error: HttpErrorResponse) => {
                this.loadData$.next(false);
                this.error$.next(error);
                return of(null);
              })
            );
        })
      );
  }

  public getOffensesByAuthorsData(): Observable<IOffenseByAuthors> {
    this.loadData$.next(false);

    const subscribers = combineLatest([
      this.update$
        .pipe(
          filter(chartInfo => chartInfo),
          tap(() => this.error$.next(null)),
        ),
    ]);

    return subscribers
      .pipe(
        switchMap(() => {

          return this.getOffenseByAuthorsDataset()
            .pipe(
              tap(() => this.loadData$.next(false)),
              catchError((error: HttpErrorResponse) => {
                this.loadData$.next(false);
                this.error$.next(error);
                return of(null);
              })
            );
        })
      );
  }

  public getOffenseByLinksData(): Observable<IOffenseByLink> {
    this.loadData$.next(false);

    const subscribers = combineLatest([
      this.update$
        .pipe(
          filter(chartInfo => chartInfo),
          tap(() => this.error$.next(null)),
        ),
    ]);

    return subscribers
      .pipe(
        switchMap(() => {

          return this.getOffenseByLinkDataset()
            .pipe(
              tap(() => this.loadData$.next(false)),
              catchError((error: HttpErrorResponse) => {
                this.loadData$.next(false);
                this.error$.next(error);
                return of(null);
              })
            );
        })
      );
  }

  public getOffenseByResponsiblePersonData(): Observable<IOffenseByResponsiblePerson> {
    this.loadData$.next(false);

    const subscribers = combineLatest([
      this.update$
        .pipe(
          filter(chartInfo => chartInfo),
          tap(() => this.error$.next(null)),
        ),
    ]);

    return subscribers
      .pipe(
        switchMap(() => {

          return this.getOffenseByResponsiblePersonDataset()
            .pipe(
              tap(() => this.loadData$.next(false)),
              catchError((error: HttpErrorResponse) => {
                this.loadData$.next(false);
                this.error$.next(error);
                return of(null);
              })
            );
        })
      );
  }
}
