import { ChartDataSets } from 'chart.js';

export interface IOffenseByAuthors {
  dataSet: Array<ChartDataSets>;
  dataLabels: Array<string>;
}
