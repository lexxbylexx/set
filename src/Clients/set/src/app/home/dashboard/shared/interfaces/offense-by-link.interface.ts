import { ChartDataSets } from 'chart.js';

export interface IOffenseByLink {
  dataSet: Array<ChartDataSets>;
  dataLabels: Array<string>;
}
