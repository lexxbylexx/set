import { ChartDataSets } from 'chart.js';

export interface IOffenseByResponsiblePerson {
  dataSet: Array<ChartDataSets>;
  dataLabels: Array<string>;
}
