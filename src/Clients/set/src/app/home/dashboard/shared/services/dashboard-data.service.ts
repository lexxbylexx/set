import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { retryWhen, delay, take, concat } from 'rxjs/operators';

import { OffenseChartUrls } from '@set-app/core/urls/offense';

import { IOffenseOnObject, IOffenseByAuthors, IOffenseByLink, IOffenseByResponsiblePerson } from '../interfaces';

@Injectable()
export class DashboardDataService {

  constructor (
    protected readonly http: HttpClient,
  ) {
  }

  /**
   * Делает запрос к серверу и
   * возвращает данные для графика кол-во предписаний на объекте
   */
  protected getOffenseObjectDataset(): Observable<IOffenseOnObject> {
    return this.http.get<IOffenseOnObject>(OffenseChartUrls.offenseOnObject)
      .pipe(
        retryWhen(errors => {
          return errors
            .pipe(
              delay(500),
              take(5),
              concat(throwError(new Error('Bad request'))),
            );
        }),
      );
  }

  /**
   * Делает запрос к серверу и
   * возвращает данные для графика предписания по авторам
   */
  protected getOffenseByAuthorsDataset(): Observable<IOffenseByAuthors> {
    return this.http.get<IOffenseByAuthors>(OffenseChartUrls.offenseByAuthors)
      .pipe(
        retryWhen(errors => {
          return errors
            .pipe(
              delay(300),
              take(5),
              concat(throwError(new Error('Can not get data about offense by author chart'))),
            )
        })
      )
  }

  /**
   * Делает запрос к серверу и
   * возвращает данные для графика пунктов предписаний по статьям
   */
  protected getOffenseByLinkDataset(): Observable<IOffenseByLink> {
    return this.http.get<IOffenseByLink>(OffenseChartUrls.offenseByLinks)
      .pipe(
        retryWhen(errors => {
          return errors
            .pipe(
              delay(300),
              take(5),
              concat(throwError(new Error('Can not get data about offense by links chart')))
            )
        })
      )
  }

  /**
   * Делает запрос к серверу и
   * возвращает данные для графика пунктов предписаний по ответственным лицам
   */
  protected getOffenseByResponsiblePersonDataset(): Observable<IOffenseByResponsiblePerson> {
    return this.http.get<IOffenseByResponsiblePerson>(OffenseChartUrls.offenseByResponsiblePerson)
      .pipe(
        retryWhen(errors => {
          return errors
            .pipe(
              delay(300),
              take(5),
              concat(throwError(new Error('Can not get data about offense by responsible persons chart')))
            )
        })
      );
  }
}
