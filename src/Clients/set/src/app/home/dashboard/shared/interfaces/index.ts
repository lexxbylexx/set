export * from './offense-on-object.interface';
export * from './offense-by-authors.interface';
export * from './offense-by-link.interface';
export * from './offense-by-responsible-person.interface';
