import { Component, OnInit } from '@angular/core';
import { OffenseService } from '../../shared/services';

@Component({
  selector: 'set-offence-controls',
  templateUrl: './offence-controls.component.html',
  styleUrls: ['./offence-controls.component.scss']
})
export class OffenceControlsComponent implements OnInit {

  constructor(
    private readonly offenceService: OffenseService,
  ) { }

  public refreshList(): void {
    this.offenceService.loadData$.next(true);
    this.offenceService.updateList$.next(true);
  }

  public createOffense(): void {
    this.offenceService.openOffenseModal();
  }

  public toggleFilters(): void {
    const filtersVisible = this.offenceService.showFilters$.value;
    this.offenceService.showFilters$.next(!filtersVisible);
  }

  public ngOnInit(): void {
  }

}
