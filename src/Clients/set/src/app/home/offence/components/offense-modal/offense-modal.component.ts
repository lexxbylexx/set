import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { NzModalRef } from 'ng-zorro-antd';

import { IOffenseDto } from '../../shared/interfaces';

import { defaultDateFormat } from '@set-app/core/const';

@Component({
  selector: 'set-offense-modal',
  templateUrl: './offense-modal.component.html',
  styleUrls: ['./offense-modal.component.scss']
})
export class OffenseModalComponent implements OnInit {
  public readonly dateFormat = defaultDateFormat;
  @Input() public offense: IOffenseDto;

  public offenseForm: FormGroup;

  constructor(
    private readonly modal: NzModalRef,
    private readonly fb: FormBuilder,
  ) { }

  public ngOnInit(): void {
    this.initForm(this.offense);
  }

  private initForm(model?: IOffenseDto): void {
    this.offenseForm = this.fb.group({
      id: [model ? model.id : undefined],
      issuedDate: [model ? model.issuedDate : new Date(), Validators.required],
      numberOffense: [model ? model.numberOffense : undefined],
      personIssued: [model ? model.personIssued : undefined, Validators.required]
    });
  }

  public submit(): void {
    if (this.offenseForm.invalid) {
      return;
    }

    this.modal.destroy(this.offenseForm.value);
  }

  public close(): void {
    this.modal.destroy();
  }
}
