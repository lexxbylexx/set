import { Component, OnInit, Input } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd';
import { IOffenseItemDto } from '../../shared/interfaces';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { defaultDateFormat } from '@set-app/core/const';

@Component({
  selector: 'set-offense-item-complete-dialog',
  templateUrl: './offense-item-complete-dialog.component.html',
  styleUrls: ['./offense-item-complete-dialog.component.scss']
})
export class OffenseItemCompleteDialogComponent implements OnInit {
  public readonly dateFormat = defaultDateFormat;
  @Input() public offenseItem: IOffenseItemDto;
  public form: FormGroup;

  constructor(
    private readonly dialog: NzModalRef,
    private readonly fb: FormBuilder,
  ) { }

  private initForm(): void {
    this.form = this.fb.group({
      id: [this.offenseItem.id],
      offenseId: [this.offenseItem.offenseId],
      number: [this.offenseItem.number],
      heading: [this.offenseItem.heading, Validators.required],
      prescription: [this.offenseItem.prescription],
      link: [this.offenseItem.link],
      object: [this.offenseItem.object, Validators.required],
      action: [this.offenseItem.action],
      responsiblePerson: [this.offenseItem.responsiblePerson],
      requiredDate: [this.offenseItem.requiredDate, Validators.required],
      actualDate: [this.offenseItem.actualDate, Validators.required],
      status: [true],
      comment: [this.offenseItem.comment],
    });
  }

  public submit(): void {
    if (this.form.valid) {
      this.dialog.destroy(this.form.value);
    }

    this.form.markAllAsTouched();
  }

  public cancel(): void {
    this.dialog.destroy();
  }

  ngOnInit() {
    this.initForm();
  }

}
