import { Component, OnInit } from '@angular/core';
import { IOffenseResponse, IOffenseDto } from 'src/app/home/offence/shared/interfaces';
import { OffenseService } from 'src/app/home/offence/shared/services';
import { OffenseOrderByEnum } from '@set-app/home/offence/components/offence-root/shared/enums';
import { IKeyValue } from '@set-app/shared/interfaces';

@Component({
  selector: 'set-offence-table',
  templateUrl: './offence-table.component.html',
  styleUrls: ['./offence-table.component.scss']
})
export class OffenceTableComponent implements OnInit {

  public offenseResponse: IOffenseResponse;
  public pageItemNumber: number;
  public error$ = this.offenseService.error$;
  public loading$ = this.offenseService.loadData$;
  public offenseOrderByEnum = OffenseOrderByEnum;
  constructor(
    private readonly offenseService: OffenseService,
  ) { }

  public getStatus(percent: number): string {
    return this.offenseService.getStatus(percent);
  }

  public ngOnInit(): void {
    this.offenseService.getPaginatedList()
      .subscribe((response: IOffenseResponse) => {
        this.offenseResponse = response;
        this.pageItemNumber = this.calcPageItemNumber(response);
      });
  }

  private calcPageItemNumber(serverResponse: IOffenseResponse): number {
    if (serverResponse) {
      return (serverResponse.currentPage - 1) * serverResponse.itemsPerPage + 1;
    }

    return 0;
  }

  public updateOffense(model: IOffenseDto): void {
    this.offenseService.openOffenseModal(model);
  }

  public showOffense(offenseId: string): void {
    this.offenseService.getOffenseDetails(offenseId)
      .subscribe((offense: IOffenseDto) => {
        this.offenseService.openOffenseViewModal(offense);
      });
  }

  public delete(offence: IOffenseDto): void {
    this.offenseService.confirmDelete(offence);
  }

  public onPageChange(page: number): void {
    const filters = this.offenseService.filters$.value;
    filters.page = page;

    this.offenseService.filters$
      .next(filters);
  }

  public offenseSort(sort: IKeyValue): void {
    const filters = this.offenseService.filters$.value;
    if (sort.value) {
      filters.orderBy = sort.key;
      filters.asc = sort.value;
    } else {
      filters.orderBy = this.offenseOrderByEnum.Unordered;
      filters.asc = 'ascend';
    }

    this.offenseService.filters$.next(filters);
  }
}
