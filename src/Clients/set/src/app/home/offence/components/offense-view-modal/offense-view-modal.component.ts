import { Component, Input } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd';

import { IOffenseDto } from '../../shared/interfaces';

@Component({
  selector: 'set-offense-view-modal',
  templateUrl: './offense-view-modal.component.html',
  styleUrls: ['./offense-view-modal.component.scss'],
})
export class OffenseViewModalComponent {
  @Input() public offense: IOffenseDto;

  constructor(
    private readonly modal: NzModalRef,
  ) {
  }

  public close(): void {
    this.modal.destroy();
  }
}
