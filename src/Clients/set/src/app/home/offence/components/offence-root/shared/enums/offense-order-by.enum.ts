export enum OffenseOrderByEnum {
  Unordered,
  OffenseNumber,
  OffenseIssuedDate,
  OffenseAuthor,
  OffenseObject,
}
