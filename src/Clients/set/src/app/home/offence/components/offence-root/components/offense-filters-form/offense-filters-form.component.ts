import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { defaultDateFormat } from '@set-app/core/const';

import { OffenseService } from '@set-app/home/offence/shared/services';

import { IFilters } from '@set-app/home/offence/shared/interfaces';
import { FiltersModel } from '../../shared/models';

@Component({
  selector: 'set-offense-filters-form',
  templateUrl: './offense-filters-form.component.html',
  styleUrls: ['./offense-filters-form.component.scss']
})
export class OffenseFiltersFormComponent implements OnInit, OnDestroy {
  public readonly dateFormat = defaultDateFormat;

  public filterForm: FormGroup;

  constructor(
    private readonly os: OffenseService,
    private readonly fb: FormBuilder,
  ) { }

  private initForm(): void {
    const currentFilters: IFilters = this.os.filters$.value;

    this.filterForm = this.fb.group({
      offenseNumber: [currentFilters.offenseNumber],
      fromDate: [currentFilters.dateFrom],
      toDate: [currentFilters.dateTo],
      author: [currentFilters.author],
    });
  }

  public applyFilters(): void {
    const filtersModel = new FiltersModel();
    const filters = filtersModel
      .fromFilterForm(this.filterForm.value);

    this.os.filters$.next(filters);
  }

  public resetFilters(): void {
    this.filterForm.reset();
    this.applyFilters();
  }

  public ngOnInit(): void {
    this.initForm();
  }

  public ngOnDestroy(): void {
  }
}
