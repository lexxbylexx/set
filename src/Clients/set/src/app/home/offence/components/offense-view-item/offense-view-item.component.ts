import { Component, Input } from '@angular/core';
import { IOffenseItemDto } from '@set-app/home/offence/shared/interfaces';

@Component({
  selector: 'set-offense-view-item',
  templateUrl: './offense-view-item.component.html',
  styleUrls: ['./offense-view-item.component.scss']
})
export class OffenseViewItemComponent {
  @Input() public offenseItem: IOffenseItemDto;
  @Input() public index: number;
  @Input() public total: number;
}
