import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { OffenseService } from 'src/app/home/offence/shared/services';
import { IOffenseItemDto } from 'src/app/home/offence/shared/interfaces';

@Component({
  selector: 'set-offense-details',
  templateUrl: './offense-details.component.html',
  styleUrls: ['./offense-details.component.scss']
})
export class OffenseDetailsComponent implements OnInit {

  private offenseId: string;

  public offense$ = this.offenseService.offenseDetails$;
  public dataUpdate$ = this.offenseService.loadData$;
  public error$ = this.offenseService.error$;

  constructor(
    private readonly offenseService: OffenseService,
    ar: ActivatedRoute,
  ) {
    this.offenseId = ar.snapshot.params['objectId'];
  }

  private initObjectData(id: string): void {
    this.offenseService.getOffenseDetails(id)
      .subscribe()
  }

  public createOffenseItem(): void {
    this.offenseService.openOffenseItemModal();
  }

  public updateOffenseItem(id: string): void {
    this.offenseService.getOffenseItem(id);
  }

  public deleteOffenseItem(item: IOffenseItemDto): void {
    this.offenseService.confirmDeleteItem(item)
  }

  public getStatus(percent: number): string {
    return this.offenseService.getStatus(percent);
  }

  public showItem(item: IOffenseItemDto, index: number, total: number): void {
    this.offenseService.openOffenseItemViewDialog(item, index, total);
  }

  public completeItem(offenseItem: IOffenseItemDto): void {
    this.offenseService.openOffenseItemCompleteDialog(offenseItem);
  }

  public ngOnInit(): void {
    this.initObjectData(this.offenseId);
  }
}
