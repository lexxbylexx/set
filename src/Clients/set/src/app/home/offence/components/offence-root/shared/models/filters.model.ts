import { IFilters } from '@set-app/home/offence/shared/interfaces';
import { OffenseOrderByEnum } from '../enums';

export class FiltersModel implements IFilters {
  public page: number = 1;
  public itemsPerPage: number = 10;
  public asc: string = 'ascend';
  public orderBy: OffenseOrderByEnum;
  public offenseNumber: string;
  public dateFrom: string;
  public dateTo: string;
  public fromDate: Date;
  public toDate: Date;
  public author: string;
  public offenseObject: string;

  public fromFilterForm(filterForm: FiltersModel): FiltersModel {
    let filters = new FiltersModel();
    filters.page = 1;
    filters.itemsPerPage = 10;
    filters.dateFrom = filterForm.fromDate ? filterForm.fromDate.toDateString() : null;
    filters.dateTo = filterForm.toDate ? filterForm.toDate.toDateString() : null;
    filters.offenseNumber = filterForm.offenseNumber;
    filters.offenseObject = filterForm.offenseObject;
    filters.asc = filterForm.asc;
    filters.author = filterForm.author;

    filters.orderBy = this.orderBy;
    filters.asc = this.asc;

    return filters;
  }
}
