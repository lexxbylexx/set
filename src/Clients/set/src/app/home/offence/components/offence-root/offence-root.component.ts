import { Component, OnInit } from '@angular/core';
import { OffenseService } from '../../shared/services';

@Component({
  selector: 'set-offence-root',
  templateUrl: './offence-root.component.html',
  styleUrls: ['./offence-root.component.scss']
})
export class OffenceRootComponent implements OnInit {

  public filtersVisible$ = this.os.showFilters$;

  constructor(
    private readonly os: OffenseService,
  ) { }

  public ngOnInit(): void {
  }

}
