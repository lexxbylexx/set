import { Component, OnInit, Input } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IOffenseItemDto } from '@set-app/home/offence/shared/interfaces';
import { defaultDateFormat } from '@set-app/core/const';

@Component({
  selector: 'set-offense-item-modal',
  templateUrl: './offense-item-modal.component.html',
  styleUrls: ['./offense-item-modal.component.scss']
})
export class OffenseItemModalComponent implements OnInit {
  public readonly dateFormat = defaultDateFormat;

  @Input() public offenseItem: IOffenseItemDto;
  @Input() public offenseId: string;

  public offenseItemForm: FormGroup;

  constructor(
    private readonly modal: NzModalRef,
    private readonly fb: FormBuilder,
  ) { }

  private initForm(model: any): void {
    this.offenseItemForm = this.fb.group({
      id: [model ? model.id : undefined],
      offenseId: [model ? model.offenseId : this.offenseId],
      number: [model ? model.number : undefined],
      heading: [model ? model.heading : undefined, Validators.required],
      prescription: [model ? model.prescription : undefined],
      link: [model ? model.link : undefined],
      object: [model ? model.object : undefined, Validators.required],
      action: [model ? model.action : undefined],
      responsiblePerson: [model ? model.responsiblePerson : undefined],
      requiredDate: [model ? model.requiredDate : new Date(), Validators.required],
      comment: [model ? model.comment : undefined],
    });
  }

  public submit(): void {
    this.modal.destroy(this.offenseItemForm.value);
  }

  public cancel(): void {
    this.modal.destroy();
  }

  public ngOnInit(): void {
    this.initForm(this.offenseItem);
  }
}
