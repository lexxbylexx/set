import { Component, Input } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd';
import { IOffenseItemDto } from '../../shared/interfaces';

@Component({
  selector: 'set-offense-item-view-dialog',
  templateUrl: './offense-item-view-dialog.component.html',
  styleUrls: ['./offense-item-view-dialog.component.scss']
})
export class OffenseItemViewDialogComponent {
  @Input() public item: IOffenseItemDto;
  @Input() public index: number;
  @Input() public total: number;

  constructor(
    private readonly modal: NzModalRef,
  ) {
  }

  public close(): void {
    this.modal.destroy();
  }
}
