import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OffenceRootComponent } from './components/offence-root/offence-root.component';
import { OffenceReportComponent } from './components/offence-report/offence-report.component';
import { OffenseDetailsComponent } from './components/offence-root/components/offense-details/offense-details.component';


const routes: Routes = [
  {
    path: '',
    component: OffenceRootComponent,
  },
  {
    path: 'report',
    component: OffenceReportComponent,
  },
  {
    path: 'details/:objectId',
    component: OffenseDetailsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OffenceRoutingModule { }
