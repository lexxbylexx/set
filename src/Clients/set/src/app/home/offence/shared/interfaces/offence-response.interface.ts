import { IOffenseDto } from './offence-dto.interface';

export interface IOffenseResponse {
  totalItems: number;
  itemsPerPage: number;
  currentPage: number;
  totalPages: number;
  items: Array<IOffenseDto>;
}
