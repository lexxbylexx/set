/** Интерфейс пункта предписания */
export interface IOffenseItemDto {
  id: string;

  /** Дата создания пункта предписания */
  created: string;

  /** Номер предписания */
  number: string;

  /** Область нарушения */
  heading: string;

  /** Текст предписания */
  prescription: string;

  /** Ссылка на нормативную базу */
  link: string;

  /** Объект предписания */
  object: string;

  /** Корректирующее мероприятие */
  action: string;

  /** Ответственное лицо */
  responsiblePerson: string;

  /** Назначенная дата устранения */
  requiredDate: string;

  /** Фактическая дата устранения */
  actualDate: string;

  /** Статус выполнения */
  status: string;

  /** Комментарий */
  comment: string;

  /** Идентификатор предписания */
  offenseId: string;
}
