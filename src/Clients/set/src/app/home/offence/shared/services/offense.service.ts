import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';

import { Observable, BehaviorSubject, combineLatest, of } from 'rxjs';
import { tap, catchError, flatMap } from 'rxjs/operators';

import { OffenseDataService } from './offense-data.service';
import { NzModalService } from 'ng-zorro-antd';

import { IOffenseResponse, IOffenseDto, IOffenseItemDto } from '../interfaces';

import { OffenseModalComponent } from '../../components/offense-modal/offense-modal.component';
import { OffenseViewModalComponent } from '@set-app/home/offence/components/offense-view-modal/offense-view-modal.component';

import { FiltersModel } from '../../components/offence-root/shared/models';
import { OffenseItemModalComponent } from '../../components/offense-item-modal/offense-item-modal.component';
import { OffenseItemViewDialogComponent } from '../../components/offense-item-view-dialog/offense-item-view-dialog.component';
import { OffenseItemCompleteDialogComponent } from '../../components/offense-item-complete-dialog/offense-item-complete-dialog.component';

@Injectable()
export class OffenseService extends OffenseDataService {

  public response$: BehaviorSubject<IOffenseResponse> = new BehaviorSubject<IOffenseResponse>(null);
  public offenseDetails$: BehaviorSubject<IOffenseDto> = new BehaviorSubject<IOffenseDto>(null);
  public offenseItem$: BehaviorSubject<IOffenseItemDto> = new BehaviorSubject<IOffenseItemDto>(null);

  public error$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public loadData$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public updateList$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public showFilters$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public filters$: BehaviorSubject<FiltersModel> = new BehaviorSubject<FiltersModel>(new FiltersModel());

  constructor(
    protected readonly http: HttpClient,
    private readonly modalService: NzModalService,
    private readonly router: Router,
  ) {
    super(http);
  }

  public getPaginatedList(): Observable<IOffenseResponse> {
    this.loadData$.next(true);

    const subscribers = combineLatest([
      this.updateList$
        .pipe(
          tap(() => this.error$.next(false))
        ),
      this.filters$
      .pipe(
        tap(() => this.error$.next(false))
      ),
    ]);

    return subscribers
      .pipe(
        flatMap(([_, filters]) => {
          return this.getList(filters)
            .pipe(
              tap((response: IOffenseResponse) => {
                this.loadData$.next(false);
                this.response$.next(response);
              }),
              catchError((err: HttpErrorResponse) => {
                this.loadData$.next(false);
                this.error$.next(err);
                return of(null);
              }),
            );
        }),
      )
  }

  public modifyOffenseObject(offense: IOffenseDto): void {
    const request = offense.id
      ? this.updateOffenseObject(offense)
      : this.createObject(offense);

    request.subscribe(
      (response: IOffenseDto) => {
        this.updateList$.next(true);
        this.router.navigate(['/offense/details', response.id]);
      },
      (err: HttpErrorResponse) => {},
    )
  }

  public modifyOffenseItem(item: IOffenseItemDto): void {
    this.loadData$.next(true);

    const request = item.id
      ? this.updateOffenseItem(item)
      : this.createOffenseItem(item);

    request
      .pipe(
        tap(() => this.loadData$.next(false))
      )
      .subscribe(
        (offense: IOffenseDto) => this.offenseDetails$.next(offense),
        error => {
          this.loadData$.next(false);
          this.error$.next(error);
        },
      );
  }

  public getOffenseDetails(id: string): Observable<IOffenseDto> {
    this.error$.next(null);
    this.offenseDetails$.next(null);

    return this.get(id)
      .pipe(
        tap((offense: IOffenseDto) => this.offenseDetails$.next(offense)),
        catchError((err: HttpErrorResponse) => {
          this.error$.next(err);
          return of(null);
        })
      );
  }

  private deleteOffenseObject(id: string): void {
    this.deleteObject(id)
      .subscribe(() => this.updateList$.next(true));
  }

  public getOffenseItem(id: string): void {
    this.getItemById(id)
      .pipe(
        tap((item: IOffenseItemDto) => this.offenseItem$.next(item)),
      )
      .subscribe((item: IOffenseItemDto) => this.openOffenseItemModal(item));
  }

  public offenseItemDelete(id: string): void {
    this.deleteOffenseItem(id)
      .subscribe((offense: IOffenseDto) => {
        this.offenseDetails$.next(offense);
      })
  }

  public openOffenseModal(offense?: IOffenseDto): void {
    const modal = this.modalService.create({
      nzWidth: 700,
      nzContent: OffenseModalComponent,
      nzComponentParams: {
        offense,
      },
      nzFooter: null,
    });

    modal.afterClose
      .subscribe(value => {
        if (value) {
          this.modifyOffenseObject(value);
        }
      });
  }

  /**
   * Открывает модальное окно просмотра предписания
   * @param offense Идентификатор предписания
   */
  public openOffenseViewModal(offense: IOffenseDto): void {
    this.modalService.create({
      nzWidth: 980,
      nzContent: OffenseViewModalComponent,
      nzComponentParams: {
        offense,
      }
    });
  }

  public openOffenseItemViewDialog(item: IOffenseItemDto, index: number, total:number): void {
    this.modalService.create({
      nzWidth: 980,
      nzContent: OffenseItemViewDialogComponent,
      nzComponentParams: {
        item,
        index,
        total,
      }
    })
  }

  public openOffenseItemModal(offenseItem?: IOffenseItemDto): void {
    const offenseId = this.offenseDetails$.value.id;

    const itemModal = this.modalService.create({
      nzWidth: 800,
      nzContent: OffenseItemModalComponent,
      nzComponentParams: {
        offenseItem,
        offenseId,
      },
      nzFooter: null,
    });

    itemModal.afterClose
      .subscribe((value: IOffenseItemDto) => {
        if (!value) {
          return;
        }

        this.modifyOffenseItem(value);
      });
  }

  public openOffenseItemCompleteDialog(offenseItem: IOffenseItemDto): void {
    const dialog = this.modalService.create({
      nzWidth: 800,
      nzTitle: 'Завершить предписание',
      nzContent: OffenseItemCompleteDialogComponent,
      nzComponentParams: {
        offenseItem,
      },
    });

    dialog.afterClose
      .subscribe(value => {
        if (value) {
          this.modifyOffenseItem(value);
        }
      });
  }

  public confirmDelete(offense: IOffenseDto): void {
    this.modalService.warning({
      nzTitle: 'Удаление предписания',
      nzContent: `Предписание за номером "${offense.numberOffense}" и все его пункты будут удалены.`,
      nzOkText: 'Удалить',
      nzOkType: 'danger',
      nzOnOk: () => this.deleteOffenseObject(offense.id),
      nzCancelText: 'Отмена',
    })
  }

  public confirmDeleteItem(offenseItem: IOffenseItemDto): void {
    this.modalService.warning({
      nzTitle: 'Удаление пункта предписания',
      nzContent: `Пункт предписания ${offenseItem.heading} будет удален!`,
      nzOkText: 'Удалить',
      nzOkType: 'danger',
      nzOnOk: () => this.offenseItemDelete(offenseItem.id),
      nzCancelText: 'Отмена'
    })
  }

  public getStatus(percent: number): string {
    let result = 'normal';

    if (percent === 0) {
      result = 'exception';
    } else if (percent < 100) {
      result = 'active';
    } else if (percent === 100) {
      result = 'success';
    }

    return result;
  }
}
