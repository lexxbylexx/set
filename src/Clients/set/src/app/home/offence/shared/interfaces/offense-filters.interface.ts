import { OffenseOrderByEnum } from '../../components/offence-root/shared/enums';

export interface IFilters {
  page: number;
  itemsPerPage: number;
  asc: string;
  orderBy: OffenseOrderByEnum;
  offenseNumber: string;
  dateFrom: string;
  dateTo: string;
  author: string;
  offenseObject: string;
}
