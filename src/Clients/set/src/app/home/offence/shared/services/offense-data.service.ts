import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { IOffenseResponse } from '../interfaces/offence-response.interface';
import { OffenseUrls } from 'src/app/core/urls/offense';
import { IOffenseDto } from '../interfaces';
import { OffenseItemDataService } from './offense-item-data.service';
import { retryWhen, delay, take, concat } from 'rxjs/operators';

export class OffenseDataService extends OffenseItemDataService {

  constructor(
    protected readonly http: HttpClient,
  ) {
    super(http);
  }

  protected getList(payload: any): Observable<IOffenseResponse> {
    let params = new HttpParams();

    params = Object.keys(payload)
      .filter((key: string) => !!payload[key])
      .reduce((acc: HttpParams, key: string) => acc.append(key, payload[key]), params);

    return this.http.get<IOffenseResponse>(OffenseUrls.offenseList, { params })
      .pipe(
        retryWhen(errors => {
          return errors
            .pipe(
              delay(500),
              take(5),
              concat(throwError(new Error('Bad request'))),
            );
        }),
      );
  }

  protected get(id: string): Observable<IOffenseDto> {
    return this.http.get<IOffenseDto>(`${OffenseUrls.offense}/${id}`)
    .pipe(
      retryWhen(errors => {
        return errors.pipe(
          delay(500),
          take(5),
          concat(throwError(new Error('Bad request'))),
        );
      }),
    );
  }

  protected createObject(offense: IOffenseDto): Observable<IOffenseDto> {
    return this.http.post<IOffenseDto>(OffenseUrls.offense, offense);
  }

  protected updateOffenseObject(offence: IOffenseDto): Observable<IOffenseDto> {
    return this.http.put<IOffenseDto>(OffenseUrls.offense, offence);
  }

  protected deleteObject(id: string): Observable<IOffenseDto> {
    return this.http.delete<IOffenseDto>(`${OffenseUrls.offense}/${id}`);
  }
}
