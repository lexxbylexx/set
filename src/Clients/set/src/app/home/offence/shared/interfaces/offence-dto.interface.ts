import { IOffenseItemDto } from './offence-item-dto.interface';

export interface IOffenseDto {
  id: string;

  /** Номер предписания */
  numberOffense: string;

  /** Автор предписания */
  personIssued: string;

  /** Дата составления */
  issuedDate: Date;

  /** Кол-во завершенных пунктов предписаний */
  completed: number;

  /** Всего пунктов в предписании */
  totalOffenseItems: number;

  /** Процент закрытия пунктов предписаний */
  percentCompleted: number;

  /** Пункты предписаний */
  offenseItems: Array<IOffenseItemDto>;
}
