import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IOffenseItemDto, IOffenseDto } from '../interfaces';
import { OffenseItemUrls } from 'src/app/core/urls/offense/offenseItem.urls';

export class OffenseItemDataService {

  constructor(
    protected readonly http: HttpClient,
  ) {
  }

  protected getItemById(id: string): Observable<IOffenseItemDto> {
    return this.http.get<IOffenseItemDto>(`${OffenseItemUrls.offenseItemController}/${id}`);
  }

  protected createOffenseItem(item: IOffenseItemDto): Observable<IOffenseDto> {
    return this.http.post<IOffenseDto>(OffenseItemUrls.offenseItemController, item);
  }

  public updateOffenseItem(item: IOffenseItemDto): Observable<IOffenseDto> {
    return this.http.put<IOffenseDto>(OffenseItemUrls.offenseItemController, item);
  }

  public deleteOffenseItem(id: string): Observable<IOffenseDto> {
    return this.http.delete<IOffenseDto>(`${OffenseItemUrls.offenseItemController}/${id}`);
  }
}
