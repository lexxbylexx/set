import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OffenceRoutingModule } from './offence-routing.module';
import { OffenceRootComponent } from './components/offence-root/offence-root.component';
import { OffenceControlsComponent } from './components/offence-controls/offence-controls.component';

import {
  NzButtonModule,
  NzIconModule,
  NzTableModule,
  NzPaginationModule,
  NzModalModule,
  NzSpinModule,
  NzFormModule,
  NzInputModule,
  NzStepsModule,
  NzGridModule,
  NzDescriptionsModule,
  NzPageHeaderModule,
  NzDatePickerModule,
  NzToolTipModule,
  NzProgressModule,
} from 'ng-zorro-antd';

import { OffenceReportComponent } from './components/offence-report/offence-report.component';
import { OffenceTableComponent } from './components/offence-root/components/offence-table/offence-table.component';
import { OffenseService } from './shared/services';
import { OffenseModalComponent } from './components/offense-modal/offense-modal.component';
import { ReactiveFormsModule } from '@angular/forms';
import { OffenseDetailsComponent } from './components/offence-root/components/offense-details/offense-details.component';
import { OffenseFiltersFormComponent } from './components/offence-root/components/offense-filters-form/offense-filters-form.component';
import { OffenseViewModalComponent } from './components/offense-view-modal/offense-view-modal.component';
import { OffenseViewItemComponent } from './components/offense-view-item/offense-view-item.component';
import { OffenseItemModalComponent } from './components/offense-item-modal/offense-item-modal.component';
import { OffenseItemViewDialogComponent } from './components/offense-item-view-dialog/offense-item-view-dialog.component';
import { OffenseItemCompleteDialogComponent } from './components/offense-item-complete-dialog/offense-item-complete-dialog.component';

@NgModule({
  declarations: [
    OffenceRootComponent,
    OffenceControlsComponent,
    OffenceReportComponent,
    OffenceTableComponent,
    OffenseModalComponent,
    OffenseDetailsComponent,
    OffenseItemModalComponent,
    OffenseFiltersFormComponent,
    OffenseViewModalComponent,
    OffenseViewItemComponent,
    OffenseItemViewDialogComponent,
    OffenseItemCompleteDialogComponent,
  ],
  imports: [
    CommonModule,
    OffenceRoutingModule,
    ReactiveFormsModule,

    NzButtonModule,
    NzIconModule,
    NzTableModule,
    NzPaginationModule,
    NzModalModule,
    NzSpinModule,
    NzFormModule,
    NzInputModule,
    NzDatePickerModule,
    NzStepsModule,
    NzGridModule,
    NzDescriptionsModule,
    NzPageHeaderModule,
    NzToolTipModule,
    NzProgressModule,
  ],
  providers: [
    OffenseService,
  ],
  entryComponents: [
    OffenseModalComponent,
    OffenseItemModalComponent,
    OffenseViewModalComponent,
    OffenseItemViewDialogComponent,
    OffenseItemCompleteDialogComponent,
  ]
})
export class OffenceModule { }
