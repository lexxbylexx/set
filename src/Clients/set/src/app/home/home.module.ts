import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomePageRootComponent } from './components/home-page-root/home-page-root.component';
import {
  NzLayoutModule,
  NzIconModule,
  NzMenuModule,
  NzAvatarModule,
  NzDropDownModule,
} from 'ng-zorro-antd';
import { HeaderContentComponent } from './components/header-content/header-content.component';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { SideMenuUserComponent } from './components/side-menu-user/side-menu-user.component';


@NgModule({
  declarations: [
    HomePageRootComponent,
    HeaderContentComponent,
    SideMenuComponent,
    SideMenuUserComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,

    NzLayoutModule,
    NzIconModule,
    NzMenuModule,
    NzAvatarModule,
    NzDropDownModule,
  ]
})
export class HomeModule { }
