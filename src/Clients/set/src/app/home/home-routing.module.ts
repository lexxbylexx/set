import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageRootComponent } from './components/home-page-root/home-page-root.component';


const routes: Routes = [
  {
    path: '',
    component: HomePageRootComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
      },
      {
        path: 'offense',
        loadChildren: () => import('./offence/offence.module').then(m => m.OffenceModule),
      },
      {
        path: '',
        redirectTo: 'dashboard'
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
