﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Common
{
    public class ContextBase<TContext> where TContext : DbContext
    {
        protected readonly TContext Context;
        protected readonly IMapper Mapper;

        public ContextBase(
                TContext context,
                IMapper mapper
            )
        {
            Context = context;
            Mapper = mapper;
        }
    }
}