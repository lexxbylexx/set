﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class UpdateOffenseObject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Object",
                table: "Offenses");

            migrationBuilder.AddColumn<string>(
                name: "Object",
                table: "OffenseItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Object",
                table: "OffenseItems");

            migrationBuilder.AddColumn<string>(
                name: "Object",
                table: "Offenses",
                type: "text",
                nullable: true);
        }
    }
}
