﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class InitialMigrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Offenses",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    NumberOffense = table.Column<string>(nullable: true),
                    PersonIssued = table.Column<string>(nullable: true),
                    Object = table.Column<string>(nullable: true),
                    IssuedDate = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Offenses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OffenseItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Number = table.Column<string>(nullable: true),
                    Heading = table.Column<string>(nullable: true),
                    Prescription = table.Column<string>(nullable: true),
                    Link = table.Column<string>(nullable: true),
                    Action = table.Column<string>(nullable: true),
                    ResponsiblePerson = table.Column<string>(nullable: true),
                    RequiredDate = table.Column<DateTimeOffset>(nullable: false),
                    ActualDate = table.Column<DateTimeOffset>(nullable: false),
                    Status = table.Column<bool>(nullable: false),
                    Comment = table.Column<string>(nullable: true),
                    OffenseId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OffenseItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OffenseItems_Offenses_OffenseId",
                        column: x => x.OffenseId,
                        principalTable: "Offenses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OffenseItems_OffenseId",
                table: "OffenseItems",
                column: "OffenseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OffenseItems");

            migrationBuilder.DropTable(
                name: "Offenses");
        }
    }
}
