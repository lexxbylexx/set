﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.DbConfigurations.Offenses
{
    public class OffenseItemConfiguration : IEntityTypeConfiguration<OffenseItemEntity>
    {
        public void Configure(EntityTypeBuilder<OffenseItemEntity> builder)
        {
            builder
                .HasOne<OffenseEntity>(o => o.Offense)
                .WithMany(o => o.OffenseItems)
                .HasForeignKey(o => o.OffenseId);
        }
    }
}