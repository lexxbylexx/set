﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.DbConfigurations.Offenses
{
    public class OffensesConfiguration : IEntityTypeConfiguration<OffenseEntity>
    {
        public void Configure(EntityTypeBuilder<OffenseEntity> builder)
        {
            builder
                .HasMany<OffenseItemEntity>(o => o.OffenseItems)
                .WithOne(o => o.Offense)
                .HasForeignKey(o => o.OffenseId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}