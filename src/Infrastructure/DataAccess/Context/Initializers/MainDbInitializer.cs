﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace DataAccess.Context.Initializers
{
    public static class MainDbInitializer
    {
        private static MainContext _context;

        public static void Seed(IServiceScope scope)
        {
            _context = scope.ServiceProvider.GetRequiredService<MainContext>();

            _context.Database.Migrate();
        }
    }
}