﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Context
{
    public class MainContext: DbContext
    {
        public DbSet<OffenseEntity> Offenses { get; set; }
        public DbSet<OffenseItemEntity> OffenseItems { get; set; }

        public MainContext(DbContextOptions<MainContext> options): base(options) {}

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(typeof(MainContext).Assembly);
        }
    }
}