﻿using System.Reflection;
using DataAccess.Context;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Factories.MainDatabase
{
    public class DesignTimeMainDbContextFactory : DesignTimeDbContextFactoryBase<MainContext>
    {
        public DesignTimeMainDbContextFactory(): base("MainDb", typeof(MainContext).GetTypeInfo().Assembly.GetName().Name)
        {
        }

        protected override MainContext CreateNewInstance(DbContextOptions<MainContext> options)
        {
            return new MainContext(options);
        }
    }
}