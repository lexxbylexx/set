﻿using System;

namespace Domain.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string name, object key)
            : base($"Запись \"{name}\" с ключом ({key}) не найдена.")
        {
            
        }
    }
}