﻿using System;

namespace Domain.DTO.Offense
{
    public class OffenseItemDto
    {
        public Guid Id { get; set; }

        public DateTime Created { get; set; }

        /// <summary>
        /// Номер пункта предписания
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Область нарушения
        /// </summary>
        public string Heading { get; set; }

        /// <summary>
        /// Текст предписания
        /// </summary>
        public string Prescription { get; set; }

        /// <summary>
        /// Ссылка на нормативную базу
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// Объект
        /// </summary>
        public string Object { get; set; }

        /// <summary>
        /// Корректирующее мероприятие
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// Ответственное лицо
        /// </summary>
        public string ResponsiblePerson { get; set; }

        /// <summary>
        /// Назначенная дата устранения
        /// </summary>
        public string RequiredDate { get; set; }

        /// <summary>
        /// Фактическая дата устранения
        /// </summary>
        public string ActualDate { get; set; }

        /// <summary>
        /// Статус выполнения
        /// </summary>
        public bool Status { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Идентификатор предписания
        /// </summary>
        public Guid OffenseId { get; set; }
    }
}