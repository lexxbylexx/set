﻿using System;
using System.Collections.Generic;

namespace Domain.DTO.Offense
{
    public class OffenseDto
    {
        public Guid? Id { get; set; }

        /// <summary>
        /// Номер предписания
        /// </summary>
        public string NumberOffense { get; set; }

        /// <summary>
        /// Лицо, выдавшее предписание
        /// </summary>
        public string PersonIssued { get; set; }

        /// <summary>
        /// Дата выдачи
        /// </summary>
        public string IssuedDate { get; set; }

        /// <summary>
        /// Пунктов предписаний завершено
        /// </summary>
        public int Completed { get; set; }

        /// <summary>
        /// Всего пунктов предписаний
        /// </summary>
        public int TotalOffenseItems { get; set; }

        /// <summary>
        /// Процентов завершено
        /// </summary>
        public double PercentCompleted { get; set; }

        /// <summary>
        /// Пункты предписания
        /// </summary>
        public List<OffenseItemDto> OffenseItems { get; set; }
    }
}