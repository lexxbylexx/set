﻿using System.Collections.Generic;

namespace Domain.DTO.Chart.OffenseOnObjects
{
    public class OffenseOnObjectChartDataSet
    {
        public List<OffenseOnObjectChartData> DataSet { get; set; }
        public List<string> DataLabels { get; set; }
    }
}