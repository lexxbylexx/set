﻿using System.Collections.Generic;

namespace Domain.DTO.Chart.OffenseOnObjects
{
    public class OffenseOnObjectChartData
    {
        public List<int> Data { get; set; }
        public string Label { get; set; }
    }
}