﻿using System.Collections.Generic;

namespace Domain.DTO.Chart.OffenseByResponsiblePerson
{
    public class OffenseByResponsiblePersonChartData
    {
        public List<int> Data { get; set; }
        public string Label { get; set; }
    }
}