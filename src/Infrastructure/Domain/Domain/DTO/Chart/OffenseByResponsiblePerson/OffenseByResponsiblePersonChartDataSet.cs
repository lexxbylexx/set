﻿using System.Collections.Generic;

namespace Domain.DTO.Chart.OffenseByResponsiblePerson
{
    public class OffenseByResponsiblePersonChartDataSet
    {
        public List<OffenseByResponsiblePersonChartData> DataSet { get; set; }
        public List<string> DataLabels { get; set; }
    }
}