﻿using System.Collections.Generic;

namespace Domain.DTO.Chart.OffenseByLinks
{
    public class OffenseByLinksChartDataSet
    {
        public List<OffenseByLinksChartData> DataSet { get; set; }
        public List<string> DataLabels { get; set; }
    }
}