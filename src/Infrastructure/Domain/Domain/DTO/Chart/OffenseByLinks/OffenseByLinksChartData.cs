﻿using System.Collections.Generic;

namespace Domain.DTO.Chart.OffenseByLinks
{
    public class OffenseByLinksChartData
    {
        public List<int> Data { get; set; }
        public string Label { get; set; }

    }
}