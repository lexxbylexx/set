﻿using System.Collections.Generic;

namespace Domain.DTO.Chart.OffenseByAuthors
{
    public class OffenseByAuthorsChartDataSet
    {
        public List<OffenseByAuthorsChartData> DataSet { get; set; }
        public List<string> DataLabels { get; set; }

    }
}