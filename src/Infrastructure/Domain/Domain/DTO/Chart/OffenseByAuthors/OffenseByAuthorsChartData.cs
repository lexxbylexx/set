﻿using System.Collections.Generic;

namespace Domain.DTO.Chart.OffenseByAuthors
{
    public class OffenseByAuthorsChartData
    {
        public List<int> Data { get; set; }
        public string Label { get; set; }
    }
}