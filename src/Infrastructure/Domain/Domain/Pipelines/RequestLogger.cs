﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MediatR.Pipeline;
using Serilog;
using Serilog.Events;

namespace Domain.Pipelines
{
    /// <summary>
    /// Логирование входящих запросов к контроллерам
    /// </summary>
    /// <typeparam name="TRequest"></typeparam>
    public class RequestLogger<TRequest> : IRequestPreProcessor<TRequest>
    {
        private readonly ILogger _logger;

        public RequestLogger(ILogger logger)
        {
            _logger = logger;
        }

        public Task Process(TRequest request, CancellationToken cancellationToken)
        {
            var name = typeof(IRequest).Name;

            _logger.Write<IRequest>(LogEventLevel.Information, $"Запрос {name}", null);

            return Task.CompletedTask;
        }
    }
}