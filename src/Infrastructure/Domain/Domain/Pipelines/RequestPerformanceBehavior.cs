﻿using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Serilog;
using Serilog.Events;

namespace Domain.Pipelines
{
    /// <summary>
    /// Информация о производительности системы.
    /// Все запросы длящиеся более 500мс попадают в лог. 
    /// </summary>
    /// <typeparam name="TRequest"></typeparam>
    /// <typeparam name="TResponse"></typeparam>
    public class RequestPerformanceBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly Stopwatch _timer;
        private readonly ILogger _logger;

        public RequestPerformanceBehavior(ILogger logger)
        {
            _logger = logger;
            _timer = new Stopwatch();
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            _timer.Start();

            var response = await next();

            _timer.Stop();

            if (_timer.ElapsedMilliseconds < 500) return response;

            var name = typeof(TRequest).Name;
            _logger.Write<TRequest>(LogEventLevel.Warning, $"Запрос {name} длился {_timer.ElapsedMilliseconds}ms.", request);

            return response;
        }
    }
}