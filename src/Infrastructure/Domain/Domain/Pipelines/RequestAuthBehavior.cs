﻿using System.Threading;
using System.Threading.Tasks;
using Domain.Helpers;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace Domain.Pipelines
{
    public class RequestAuthBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly TempItemsDictionary _items;

        public RequestAuthBehavior(IHttpContextAccessor contextAccessor, TempItemsDictionary dictionary)
        {
            _httpContextAccessor = contextAccessor;
            _items = dictionary;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            _items["CurrentUser"] = _httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            return await next();
        }
    }
}