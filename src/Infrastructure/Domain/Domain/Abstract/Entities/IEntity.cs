﻿using System;

namespace Domain.Abstract.Entities
{
    public interface IEntity
    {
        Guid Id { get; set; }
        DateTime Created { get; set; }
        DateTime Updated{ get; set; }
        bool IsDeleted{ get; set; }
    }
}