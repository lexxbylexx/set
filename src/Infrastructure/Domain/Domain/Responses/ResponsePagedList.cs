﻿using System;
using System.Collections.Generic;

namespace Domain.Responses
{
    /// <summary>
    /// Ответ контроллера в виде пагинированной коллекции
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResponsePagedList<T>
        where T : class
    {
        /// <summary>
        /// Всего записей
        /// </summary>
        public int TotalItems { get; set; }

        /// <summary>
        /// Записей на странице
        /// </summary>
        public int ItemsPerPage { get; set; }

        /// <summary>
        /// Текущая страница
        /// </summary>
        public int CurrentPage { get; set; }

        /// <summary>
        /// Всего страниц
        /// </summary>
        public int TotalPages => (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage);

        /// <summary>
        /// Записи
        /// </summary>
        public List<T> Items { get; set; }
    }
}