﻿using Domain.Managers.Configuration.Common;

namespace Domain.Managers.Configuration.DataBaseInitializer
{
    public class DataBaseInitializerConfiguration
    {
        public ConnectionStrings ConnectionStrings { get; set; }
    }
}