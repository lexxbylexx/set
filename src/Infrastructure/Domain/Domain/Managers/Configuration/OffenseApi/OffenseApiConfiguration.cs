﻿using Domain.Managers.Configuration.Common;

namespace Domain.Managers.Configuration.OffenseApi
{
    public class OffenseApiConfiguration
    {
        public ConnectionStrings ConnectionStrings { get; set; }
        public AllowedHosts AllowedHosts { get; set; }

    }
}