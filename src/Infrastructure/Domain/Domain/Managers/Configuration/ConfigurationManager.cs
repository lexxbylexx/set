﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace Domain.Managers.Configuration
{
    /// <summary>
    /// Менеджер конфигурации для апи
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static class ConfigurationManager<T> where T:class
    {
        /// <summary>
        /// Корень конфигурации
        /// </summary>
        public static IConfigurationRoot Root { get; private set; }

        /// <summary>
        /// Экземпляр конфигурации
        /// </summary>
        public static T Configuration => Root.Get<T>();

        /// <summary>
        /// Инициализируем конфигурацию
        /// </summary>
        public static void Initialize()
        {
            var currentEnvironment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var root = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{currentEnvironment}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();

            Root = root.Build();
        }

        /// <summary>
        /// Инициализирует конфигурацию системы на базе внешней конфигурации
        /// </summary> 
        /// <param name="configuration"></param>
        public static void Initialize(IConfiguration configuration)
        {
            var root = new ConfigurationBuilder()
                .AddConfiguration(configuration);

            Root = root.Build();
        }

        public static string GetConnectionString(string connectionName)
        {
            var connectionString =
                Environment.GetEnvironmentVariable($"ASPNETCORE_CON_STRING_{connectionName.ToUpper()}");

            return string.IsNullOrEmpty(connectionString) ? null : connectionString;
        }
    }
}