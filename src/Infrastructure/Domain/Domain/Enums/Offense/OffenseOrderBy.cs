﻿namespace Domain.Enums.Offense
{
    public enum OffenseOrderBy
    {
        Unordered,
        OffenseNumber,
        OffenseIssuedDate,
        OffenseAuthor,
        OffenseObject,
    }
}