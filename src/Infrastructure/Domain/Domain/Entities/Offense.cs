﻿using System;
using System.Collections.Generic;
using Domain.Abstract.Entities;

namespace Domain.Entities
{
    public class OffenseEntity : IEntity
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Номер предписания
        /// </summary>
        public string NumberOffense { get; set; }

        /// <summary>
        /// Лицо, выдавшее предписание
        /// </summary>
        public string PersonIssued { get; set; }

        /// <summary>
        /// Дата выдачи
        /// </summary>
        public DateTimeOffset IssuedDate { get; set; }

        /// <summary>
        /// Пункты предписания
        /// </summary>
        public List<OffenseItemEntity> OffenseItems { get; set; }
    }
}