﻿using System;
using Domain.DTO.Offense;

namespace Domain.Entities.Extensions
{
    public static class OffenseExt
    {
        public static OffenseEntity Update(this OffenseEntity entity, OffenseDto dto)
        {
            entity.Updated = DateTime.Now;

            return entity;
        }
    }
}