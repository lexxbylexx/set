﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Abstract.Entities;
using Domain.DTO.Offense;
using Domain.Entities;
using Offense.Application.CQRS.Offense;
using Offense.Application.MapperConfigurations.Actions;

namespace Offense.Application.MapperConfigurations
{
    public class OffenseMap :  Profile
    {
        public OffenseMap()
        {
            CreateMap<OffenseEntity, OffenseDto>()
                .ForMember(dto => dto.TotalOffenseItems, exp => exp.MapFrom(entity => entity.OffenseItems.Count))
                .ForMember(dto => dto.Completed, exp => exp.MapFrom(entity => entity.OffenseItems.Count(i => i.Status)))
                .ForMember(dto => dto.PercentCompleted, exp => exp.MapFrom(entity => CountPercentCompletedItems(entity)))
                .BeforeMap<SortOffenseItemsByCreated>();

            CreateMap<OffenseItemEntity, OffenseItemDto>()
                .ForMember(dto => dto.ActualDate, exp => exp.MapFrom(e => e.Status ? e.ActualDate.ToString() : ""));

            CreateMap<OffenseDto, OffenseEntity>()
                .ForMember(offense => offense.Updated, expression => expression.MapFrom(s => DateTime.Now));

            CreateMap<OffenseCreateCommand, OffenseEntity>()
                .ForMember(offense => offense.Created, expression => expression.MapFrom((dto, offense) => SetCreatedTime(offense)));
        }

        private static DateTime SetCreatedTime(IEntity entity)
        {
            return entity.Created > DateTime.MinValue ? entity.Created : DateTime.Now;
        }

        private static decimal CountPercentCompletedItems(OffenseEntity entity)
        {
            var countTotal = entity.OffenseItems.Count;
            var countCompleted = entity.OffenseItems.Count(i => i.Status);

            if (countTotal == 0 || countCompleted == 0)
            {
                return 0M;
            }

            var percent = ((decimal)countCompleted / countTotal) * 100;

            return Math.Round(percent, 2);
        }
    }
}