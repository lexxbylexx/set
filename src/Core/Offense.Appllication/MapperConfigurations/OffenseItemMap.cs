﻿using System;
using AutoMapper;
using Domain.Abstract.Entities;
using Domain.DTO.Offense;
using Domain.Entities;
using Offense.Application.CQRS.OffenseItem;

namespace Offense.Application.MapperConfigurations
{
    public class OffenseItemMap: Profile
    {
        public OffenseItemMap()
        {
            CreateMap<OffenseItemEntity, OffenseItemDto>()
                .ForMember(dto => dto.ActualDate, exp => exp.MapFrom(e => e.Status ? e.ActualDate.ToString() : ""));
                //.ForMember(dto => dto.Status, exp=>exp.MapFrom(item => OffenseStatus(item)));

            CreateMap<OffenseItemUpdateCommand, OffenseItemEntity>()
                .ForMember(entity => entity.Updated, expression => expression.MapFrom(e => DateTime.Now));

            CreateMap<OffenseItemCreateCommand, OffenseItemEntity>()
                .ForMember(entity => entity.Created, expression => expression.MapFrom((dto, entity) => SetCreatedTime(entity)));

        }

        private static bool OffenseStatus(OffenseItemEntity item)
        {
            return  item.ActualDate < item.RequiredDate;
        }

        private static DateTime SetCreatedTime(IEntity entity)
        {
            return entity.Created > DateTime.MinValue ? entity.Created : DateTime.Now;
        }

    }
}