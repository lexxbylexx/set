﻿using System.Linq;
using AutoMapper;
using Domain.DTO.Offense;
using Domain.Entities;

namespace Offense.Application.MapperConfigurations.Actions
{
    public class SortOffenseItemsByCreated: IMappingAction<OffenseEntity, OffenseDto>
    {
        public void Process(OffenseEntity source, OffenseDto destination, ResolutionContext context)
        {
            if (source.OffenseItems == null) return;

            var items = source.OffenseItems
                .Where(o => !o.IsDeleted)
                .OrderBy(s => s.Created);

            source.OffenseItems = items.ToList();
        }
    }
}