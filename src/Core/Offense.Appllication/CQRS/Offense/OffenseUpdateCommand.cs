﻿using Domain.DTO.Offense;
using MediatR;

namespace Offense.Application.CQRS.Offense
{
    public class OffenseUpdateCommand : OffenseDto, IRequest<OffenseDto>
    {
    }
}