﻿using System;
using Domain.DTO.Offense;
using MediatR;

namespace Offense.Application.CQRS.Offense
{
    public class OffenseQuery : IRequest<OffenseDto>
    {
        public Guid Id { get; set; }
    }
}