﻿using System;
using Domain.DTO.Offense;
using Domain.Enums.Offense;
using Domain.Responses;
using MediatR;

namespace Offense.Application.CQRS.Offense
{
    /// <summary>
    /// Модель запроса для списка с пагинацией
    /// с полями для поиска и сортировки
    /// </summary>
    public class OffenseListQuery : IRequest<ResponsePagedList<OffenseDto>>
    {
        /// <summary>
        /// Текущая страница
        /// </summary>
        public int Page { get; set; } = 0;

        /// <summary>
        /// Экземпляров на страницу
        /// </summary>
        public int ItemsPerPage { get; set; } = 50;

        /// <summary>
        /// Прямая / обратная сортировка
        /// </summary>
        public string Asc { get; set; }

        /// <summary>
        /// Номер предписания
        /// </summary>
        public string OffenseNumber { get; set; }

        /// <summary>
        /// Дата с (включительно)
        /// </summary>
        public DateTimeOffset? DateFrom { get; set; }

        /// <summary>
        /// Дата по (включительно)
        /// </summary>
        public DateTimeOffset? DateTo { get; set; }

        /// <summary>
        /// Автор предписания
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// Сортировать по полю
        /// </summary>
        public OffenseOrderBy OrderBy { get; set; }
    }
}