﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Context;
using Domain.DTO.Offense;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Offense.Application.CQRS.Offense.Handlers
{
    public class OffenseDetailHandler : IRequestHandler<OffenseQuery, OffenseDto>
    {
        private readonly MainContext _context;
        private readonly IMapper _mapper;

        public OffenseDetailHandler(MainContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<OffenseDto> Handle(OffenseQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Offenses
                .Include(o=> o.OffenseItems)
                .FirstOrDefaultAsync(o => o.Id == request.Id && o.IsDeleted == false, cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.OffenseEntity), request.Id);
            }

            var offense = _mapper.Map<OffenseDto>(entity);

            return offense;
        }
    }
}