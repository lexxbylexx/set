﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Context;
using Domain.DTO.Offense;
using Domain.Helpers;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Offense.Application.CQRS.Offense.Handlers
{
    public class OffenseCreateCommandHandler : IRequestHandler<OffenseCreateCommand, OffenseDto>
    {
        private readonly MainContext _context;
        private readonly IMapper _mapper;
        private readonly TempItemsDictionary _items;

        public OffenseCreateCommandHandler(MainContext context, TempItemsDictionary items, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            _items = items;
        }

        public async Task<OffenseDto> Handle(OffenseCreateCommand request, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<Domain.Entities.OffenseEntity>(request);

            if (string.IsNullOrEmpty(entity.NumberOffense))
            {
                var count = await _context.Offenses.CountAsync(cancellationToken);
                entity.NumberOffense = count.ToString();
            }

            await _context.Offenses.AddAsync(entity, cancellationToken);

            await _context.SaveChangesAsync(cancellationToken);

            var result = _mapper.Map<OffenseDto>(entity);

            return result;
        }
    }
}