﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Context;
using Domain.DTO.Offense;
using Domain.Entities;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Offense.Application.CQRS.Offense.Handlers
{
    public class OffenseUpdateCommandHandler : IRequestHandler<OffenseUpdateCommand, OffenseDto>
    {
        private readonly MainContext _context;
        private readonly IMapper _mapper;

        public OffenseUpdateCommandHandler(MainContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<OffenseDto> Handle(OffenseUpdateCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Offenses.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(OffenseEntity), request.Id);
            }

            entity = _mapper.Map<OffenseDto, OffenseEntity>(request, entity);

            if (string.IsNullOrEmpty(entity.NumberOffense))
            {
                var count = await _context.Offenses.CountAsync(cancellationToken);
                entity.NumberOffense = count.ToString();
            }

            await _context.SaveChangesAsync(cancellationToken);

            var result = _mapper.Map<OffenseDto>(entity);

            return result;
        }
    }
}