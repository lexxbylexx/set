﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Context;
using Domain.DTO.Offense;
using Domain.Entities;
using Domain.Enums.Offense;
using Domain.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Offense.Application.CQRS.Offense.Handlers
{
    public class OffenseListHandler : IRequestHandler<OffenseListQuery, ResponsePagedList<OffenseDto>>
    {
        private readonly MainContext _context;
        private readonly IMapper _mapper;

        public OffenseListHandler(MainContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ResponsePagedList<OffenseDto>> Handle(OffenseListQuery request, CancellationToken cancellationToken)
        {
            var result = new ResponsePagedList<OffenseDto>();
            var skip = (request.Page - 1) * request.ItemsPerPage;
            var take = request.ItemsPerPage;

            var dbItems = ApplyFilters(request);

            var selectedItems = await dbItems
                    .Skip(skip)
                    .Take(take)
                    .ToListAsync(cancellationToken);

            result.Items = _mapper.Map<List<OffenseEntity>, List<OffenseDto>>(selectedItems);

            result.ItemsPerPage = request.ItemsPerPage;

            result.CurrentPage = request.Page;

            result.TotalItems = dbItems.Count();

            return result;
        }

        #region Private methods

        private IQueryable<OffenseEntity> ApplyFilters(OffenseListQuery query)
        {
            var request = _context.Offenses
                .Include(o => o.OffenseItems)
                .Where(o => !o.IsDeleted);

            if (query.DateFrom != null)
            {
                var dateFrom = query.DateFrom.Value;
                request = request.Where(o => o.IssuedDate >= dateFrom);
            }

            if (query.DateTo != null)
            {
                var dateTo = query.DateTo.Value;
                request = request.Where(o => o.IssuedDate <= dateTo);
            }

            if (!string.IsNullOrEmpty(query.Author))
            {
                request = request
                    .Where(o => o.PersonIssued.ToLower().Contains(query.Author.ToLower()));
            }

            if (!string.IsNullOrEmpty(query.OffenseNumber))
            {
                request = request
                    .Where(o => o.NumberOffense.ToLower().Contains(query.OffenseNumber.ToLower()));
            }

            request = OrderEntitiesBy(query, request);

            return request;
        }

        private static IQueryable<OffenseEntity> OrderEntitiesBy(OffenseListQuery query, IQueryable<OffenseEntity> request)
        {
            switch (query.OrderBy)
            {
                case OffenseOrderBy.OffenseAuthor:
                {
                    request = query.Asc.Equals("ascend") ? request.OrderBy(o => o.PersonIssued) : request.OrderByDescending(o => o.PersonIssued);
                }
                    break;
                case OffenseOrderBy.OffenseIssuedDate:
                {
                    request = query.Asc.Equals("ascend") ? request.OrderBy(o => o.IssuedDate) : request.OrderByDescending(o => o.IssuedDate);
                }
                    break;
                case OffenseOrderBy.OffenseNumber:
                {
                    request = query.Asc.Equals("ascend") ? request.OrderBy(o => o.NumberOffense) : request.OrderByDescending(o => o.NumberOffense);
                }
                    break;
                case OffenseOrderBy.Unordered:
                {
                    request = request.OrderByDescending(o => o.IssuedDate);
                }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return request;
        }

        #endregion
    }
}