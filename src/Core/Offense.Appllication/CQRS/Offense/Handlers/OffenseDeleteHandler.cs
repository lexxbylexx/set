﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DataAccess.Context;
using Domain.Entities;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Offense.Application.CQRS.Offense.Handlers
{
    public class OffenseDeleteHandler : IRequestHandler<OffenseDeleteCommand>
    {
        private readonly MainContext _context;

        public OffenseDeleteHandler(MainContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(OffenseDeleteCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Offenses
                .Include(o => o.OffenseItems)
                .FirstOrDefaultAsync(o=>o.Id == request.Id, cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(OffenseEntity), request.Id);
            }

            entity.Updated = DateTime.Now;
            entity.IsDeleted = true;

            entity.OffenseItems?.ForEach(item => item.IsDeleted = true);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}