﻿using System;
using MediatR;

namespace Offense.Application.CQRS.Offense
{
    public class OffenseDeleteCommand : IRequest
    {
        public Guid Id { get; set; }
    }
}