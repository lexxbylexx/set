﻿using System;
using Domain.DTO.Offense;
using MediatR;

namespace Offense.Application.CQRS.Offense
{
    public class OffenseCreateCommand : IRequest<OffenseDto>
    {
        /// <summary>
        /// Номер предписания
        /// </summary>
        public string NumberOffense { get; set; }

        /// <summary>
        /// Лицо, выдавшее предписание
        /// </summary>
        public string PersonIssued { get; set; }

        /// <summary>
        /// Объект
        /// </summary>
        public string Object { get; set; }

        /// <summary>
        /// Дата выдачи
        /// </summary>
        public DateTimeOffset IssuedDate { get; set; }
    }
}