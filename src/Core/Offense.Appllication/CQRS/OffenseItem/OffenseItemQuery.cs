﻿using System;
using Domain.DTO.Offense;
using MediatR;

namespace Offense.Application.CQRS.OffenseItem
{
    public class OffenseItemQuery: IRequest<OffenseItemDto>
    {
        public Guid Id { get; set; }
    }
}