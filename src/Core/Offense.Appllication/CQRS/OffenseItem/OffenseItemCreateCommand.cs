﻿using System;
using Domain.DTO.Offense;
using MediatR;

namespace Offense.Application.CQRS.OffenseItem
{
    public class OffenseItemCreateCommand: IRequest<OffenseDto>
    {
        /// <summary>
        /// Номер пункта предписания
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Область нарушения
        /// </summary>
        public string Heading { get; set; }

        /// <summary>
        /// Текст предписания
        /// </summary>
        public string Prescription { get; set; }

        /// <summary>
        /// Ссылка на нормативную базу
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// Объект
        /// </summary>
        public string Object { get; set; }

        /// <summary>
        /// Корректирующее мероприятие
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// Ответственное лицо
        /// </summary>
        public string ResponsiblePerson { get; set; }

        /// <summary>
        /// Назначенная дата устранения
        /// </summary>
        public DateTimeOffset RequiredDate { get; set; }
        
        /// <summary>
        /// Комментарий
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Идентификатор предписания
        /// </summary>
        public Guid OffenseId { get; set; }
    }
}