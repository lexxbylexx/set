﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Context;
using Domain.DTO.Offense;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Offense.Application.CQRS.OffenseItem.Handlers
{
    public class OffenseItemDeleteCommandHandler: IRequestHandler<OffenseItemDeleteCommand, OffenseDto>
    {
        private readonly MainContext _context;
        private readonly IMapper _mapper;

        public OffenseItemDeleteCommandHandler(MainContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<OffenseDto> Handle(OffenseItemDeleteCommand request, CancellationToken cancellationToken)
        {
            var itemEntity = await _context.OffenseItems.FindAsync(request.Id);

            if (itemEntity == null)
            {
                throw new NotFoundException($"Пункт предписания с идентификатором {request.Id} не найдена в БД", nameof(request));
            }

            itemEntity.Updated = DateTime.Now;

            itemEntity.IsDeleted = true;

            await _context.SaveChangesAsync(cancellationToken);

            var offense = await _context.Offenses
                .Include(o=>o.OffenseItems)
                .FirstOrDefaultAsync(o=>o.Id == itemEntity.OffenseId, cancellationToken);

            var response = _mapper.Map<OffenseDto>(offense);

            return response;
        }
    }
}