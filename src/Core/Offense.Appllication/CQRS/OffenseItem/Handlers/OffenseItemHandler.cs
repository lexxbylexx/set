﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Context;
using Domain.DTO.Offense;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Offense.Application.CQRS.OffenseItem.Handlers
{
    public class OffenseItemHandler: IRequestHandler<OffenseItemQuery, OffenseItemDto>
    {
        private readonly MainContext _context;
        private readonly IMapper _mapper;

        public OffenseItemHandler(MainContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<OffenseItemDto> Handle(OffenseItemQuery request, CancellationToken cancellationToken)
        {
            var item = await _context.OffenseItems
                .Include(i=>i.Offense)
                .FirstOrDefaultAsync(i => i.Id == request.Id, cancellationToken);

            if (item == null)
            {
                throw new NotFoundException($"Пункт предписания с идентификатором {request.Id} не найден в бд.", nameof(request));
            }

            var result = _mapper.Map<OffenseItemDto>(item);

            return result;
        }
    }
}