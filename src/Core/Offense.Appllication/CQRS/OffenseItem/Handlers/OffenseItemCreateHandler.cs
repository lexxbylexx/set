﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Context;
using Domain.DTO.Offense;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Offense.Application.CQRS.OffenseItem.Handlers
{
    public class OffenseItemCreateHandler: IRequestHandler<OffenseItemCreateCommand, OffenseDto>
    {
        private readonly MainContext _context;
        private readonly IMapper _mapper;

        public OffenseItemCreateHandler(MainContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<OffenseDto> Handle(OffenseItemCreateCommand request, CancellationToken cancellationToken)
        {
            var offense = await _context.Offenses
                .Include(o=>o.OffenseItems)
                .FirstOrDefaultAsync(o => o.Id == request.OffenseId, cancellationToken);

            var number = $"{offense.NumberOffense}-{offense.OffenseItems.Count(i => !i.IsDeleted) + 1}";

            var offenseItem = _mapper.Map<Domain.Entities.OffenseItemEntity>(request);

            offenseItem.Number = number;

            await _context.OffenseItems.AddAsync(offenseItem, cancellationToken);

            await _context.SaveChangesAsync(cancellationToken);

            var offenseDto = _mapper.Map<OffenseDto>(offense);

            return offenseDto;
        }
    }
}