﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Context;
using Domain.DTO.Offense;
using Domain.Entities;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Offense.Application.CQRS.OffenseItem.Handlers
{
    public class OffenseItemUpdateCommandHandler: IRequestHandler<OffenseItemUpdateCommand, OffenseDto>
    {
        private readonly MainContext _context;
        private readonly IMapper _mapper;

        public OffenseItemUpdateCommandHandler(MainContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<OffenseDto> Handle(OffenseItemUpdateCommand request, CancellationToken cancellationToken)
        {
            var offenseEntity = await _context.Offenses
                .Include(o => o.OffenseItems)
                .FirstOrDefaultAsync(o => o.Id == request.OffenseId, cancellationToken);

            var item = await _context.OffenseItems
                .FindAsync(request.Id);

            if (offenseEntity == null)
            {
                throw new NotFoundException($"Предписание с идентификатором {request.OffenseId} не найдено в БД", nameof(request));
            }

            if (item == null)
            {
                throw new NotFoundException($"Пункт предписания с идентификатором {request.Id} не найден в БД", nameof(request));
            }

            item = _mapper.Map(request, item);

            await _context.SaveChangesAsync(cancellationToken);

            var response = _mapper.Map<OffenseDto>(offenseEntity);

            return response;
        }
    }
}