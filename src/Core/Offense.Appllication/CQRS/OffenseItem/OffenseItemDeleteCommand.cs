﻿using System;
using Domain.DTO.Offense;
using MediatR;

namespace Offense.Application.CQRS.OffenseItem
{
    public class OffenseItemDeleteCommand: IRequest<OffenseDto>
    {
        public Guid Id { get; set; }
    }
}