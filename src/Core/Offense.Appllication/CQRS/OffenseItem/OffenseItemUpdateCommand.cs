﻿using System;
using Domain.DTO.Offense;
using MediatR;

namespace Offense.Application.CQRS.OffenseItem
{
    public class OffenseItemUpdateCommand: OffenseItemCreateCommand
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Фактическая дата устранения пункта предписания
        /// </summary>
        public DateTimeOffset ActualDate { get; set; }

        /// <summary>
        /// Статус пункта предписания
        /// </summary>
        public bool Status { get; set; }

    }
}