﻿using Domain.DTO.Chart.OffenseOnObjects;
using MediatR;

namespace Offense.Application.CQRS.Charts
{
    public class OffenseOnObjectQuery: IRequest<OffenseOnObjectChartDataSet>
    {
    }
}