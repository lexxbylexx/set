﻿using Domain.DTO.Chart.OffenseByAuthors;
using MediatR;

namespace Offense.Application.CQRS.Charts
{
    public class OffenseByAuthorsQuery: IRequest<OffenseByAuthorsChartDataSet>
    {
        
    }
}