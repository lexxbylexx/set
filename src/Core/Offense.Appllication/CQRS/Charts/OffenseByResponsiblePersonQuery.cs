﻿using Domain.DTO.Chart.OffenseByResponsiblePerson;
using MediatR;

namespace Offense.Application.CQRS.Charts
{
    public class OffenseByResponsiblePersonQuery: IRequest<OffenseByResponsiblePersonChartDataSet>
    {
        
    }
}