﻿using Domain.DTO.Chart.OffenseByLinks;
using MediatR;

namespace Offense.Application.CQRS.Charts
{
    public class OffenseByLinksQuery: IRequest<OffenseByLinksChartDataSet>
    {
        
    }
}