﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataAccess.Context;
using Domain.DTO.Chart.OffenseByAuthors;
using Domain.DTO.Chart.OffenseByLinks;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Offense.Application.CQRS.Charts.handlers
{
    public class OffenseByLinksHandler: IRequestHandler<OffenseByLinksQuery, OffenseByLinksChartDataSet>
    {
        private readonly MainContext _context;

        public OffenseByLinksHandler(MainContext context)
        {
            _context = context;
        }

        public async Task<OffenseByLinksChartDataSet> Handle(OffenseByLinksQuery request, CancellationToken cancellationToken)
        {
            var result = new OffenseByLinksChartDataSet
            {
                DataSet = new List<OffenseByLinksChartData>(),
                DataLabels = new List<string>(),
            };

            var offenseItems = await _context.OffenseItems
                .ToListAsync(cancellationToken);

            FillDataSet(offenseItems, result);

            return result;
        }

        private static void FillDataSet(IEnumerable<OffenseItemEntity> offensesItems, OffenseByLinksChartDataSet result)
        {
            var offenseLinksIssued = new OffenseByLinksChartData
            {
                Data = new List<int>(),
                Label = "Предписания по нормативной базе",
            };

            var groupedOffenseItemsByLinks = offensesItems
                .GroupBy(e => e.Link, entity => entity, (link, entities) => new
                {
                    link,
                    count = entities.Count(),
                });

            foreach (var group in groupedOffenseItemsByLinks.Where(e => e.count > 5))
            {
                offenseLinksIssued.Data.Add(group.count);
                var link = group.link;

                if (string.IsNullOrEmpty(link))
                {
                    link = "Не установлено";
                }

                result.DataLabels.Add(link);
            }

            result.DataSet.Add(offenseLinksIssued);
        }
    }
}