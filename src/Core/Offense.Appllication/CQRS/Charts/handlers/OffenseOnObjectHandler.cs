﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataAccess.Context;
using Domain.DTO.Chart.OffenseOnObjects;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Offense.Application.CQRS.Charts.handlers
{
    public class OffenseOnObjectHandler : IRequestHandler<OffenseOnObjectQuery, OffenseOnObjectChartDataSet>
    {
        private readonly MainContext _context;

        public OffenseOnObjectHandler(MainContext context)
        {
            _context = context;
        }

        public async Task<OffenseOnObjectChartDataSet> Handle(OffenseOnObjectQuery request, CancellationToken cancellationToken)
        {
            var result = new OffenseOnObjectChartDataSet
            {
                DataLabels = new List<string>(),
                DataSet = new List<OffenseOnObjectChartData>(),
            };

            var offenseItems = await _context.OffenseItems
                .Include(o => o.Offense)
                .ToListAsync(cancellationToken);

            FillChartDataSet(offenseItems, result);

            return result;
        }

        private static void FillChartDataSet(IEnumerable<OffenseItemEntity> offenseItems, OffenseOnObjectChartDataSet chartDataSet)
        {
            var offenseItemsTotal = new OffenseOnObjectChartData
            {
                Data = new List<int>(),
                Label = "Всего предписаний"
            };

            var offenseItemsClosed = new OffenseOnObjectChartData
            {
                Data = new List<int>(),
                Label = "Закрыто"
            };

            var offenseItemsOpen = new OffenseOnObjectChartData
            {
                Data = new List<int>(),
                Label = "В работе"
            };


            var groupedOffenseItemsByObjects = offenseItems
                .GroupBy(i => i.Object, entity => entity, (s, entities) => new { objectName = s, offenseItems = entities });

            foreach (var group in groupedOffenseItemsByObjects)
            {
                chartDataSet.DataLabels.Add(group.objectName);
                offenseItemsTotal.Data.Add(group.offenseItems.Count());
                offenseItemsOpen.Data.Add(group.offenseItems.Count(o => o.Status));
                offenseItemsClosed.Data.Add(group.offenseItems.Count(o => o.Status == false));
            }

            chartDataSet.DataSet.AddRange(new[]
            {
                offenseItemsTotal,
                offenseItemsClosed,
                offenseItemsOpen
            });
        }
    }
}