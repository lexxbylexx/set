﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataAccess.Context;
using Domain.DTO.Chart.OffenseByAuthors;
using Domain.DTO.Chart.OffenseByResponsiblePerson;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Offense.Application.CQRS.Charts.handlers
{
    public class OffenseByResponsiblePersonHandler: IRequestHandler<OffenseByResponsiblePersonQuery, OffenseByResponsiblePersonChartDataSet>
    {
        private readonly MainContext _context;

        public OffenseByResponsiblePersonHandler(MainContext context)
        {
            _context = context;
        }

        public async Task<OffenseByResponsiblePersonChartDataSet> Handle(OffenseByResponsiblePersonQuery request, CancellationToken cancellationToken)
        {
            var result = new OffenseByResponsiblePersonChartDataSet
            {
                DataSet = new List<OffenseByResponsiblePersonChartData>(),
                DataLabels = new List<string>(),
            };

            var offenseItems = await _context.OffenseItems
                .ToListAsync(cancellationToken);

            FillDataSet(offenseItems, result);

            return result;
        }

        private static void FillDataSet(IEnumerable<OffenseItemEntity> OffenseItems,
            OffenseByResponsiblePersonChartDataSet result)
        {
            var offenseByResponsiblePersonData = new OffenseByResponsiblePersonChartData()
            {
                Label = "Предписания по ответственным лицам",
                Data = new List<int>(),
            };

            var groupedOffensesByResponsiblePersons = OffenseItems
                .GroupBy(e => e.ResponsiblePerson, entities => entities, (person, entities) => new
                {
                    person,
                    count = entities.Count(),
                });

            foreach (var group in groupedOffensesByResponsiblePersons)
            {
                offenseByResponsiblePersonData.Data.Add(group.count);
                result.DataLabels.Add(group.person);
            }

            result.DataSet.Add(offenseByResponsiblePersonData);
        }
    }
}