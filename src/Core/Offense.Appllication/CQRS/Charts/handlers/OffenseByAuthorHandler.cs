﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataAccess.Context;
using Domain.DTO.Chart.OffenseByAuthors;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Offense.Application.CQRS.Charts.handlers
{
    public class OffenseByAuthorHandler: IRequestHandler<OffenseByAuthorsQuery, OffenseByAuthorsChartDataSet>
    {
        private readonly MainContext _context;

        public OffenseByAuthorHandler(MainContext context)
        {
            _context = context;
        }

        public async Task<OffenseByAuthorsChartDataSet> Handle(OffenseByAuthorsQuery request, CancellationToken cancellationToken)
        {
            var result = new OffenseByAuthorsChartDataSet
            {
                DataSet = new List<OffenseByAuthorsChartData>(),
                DataLabels = new List<string>(),
            };

            var offenses = await _context.Offenses
                .ToListAsync(cancellationToken);

            FillChartDataSet(offenses, result);

            return result;
        }

        private static void FillChartDataSet(IEnumerable<OffenseEntity> offenses,
            OffenseByAuthorsChartDataSet dataSet)
        {
            var offenseItemsIssued = new OffenseByAuthorsChartData
            {
                Data = new List<int>(),
                Label = "Предписаний выписано",
            };

            var groupedOffenseByAuthor = offenses
                .GroupBy(p => p.PersonIssued, offense => offense, (person, offensesIssued) => new
                {
                    personIssued = person,
                    count = offensesIssued.Count(),
                });


            foreach (var group in groupedOffenseByAuthor)
            {
                dataSet.DataLabels.Add(group.personIssued);
                offenseItemsIssued.Data.Add(group.count);
            }

            dataSet.DataSet.Add(offenseItemsIssued);
        }
    }
}