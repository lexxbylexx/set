﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataAccess.Context;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace supplyConsole.UploadOffense
{
    public class UploadOffenseFromCsv
    {

        public void Upload()
        {
            var offenses = GetEntitiesFromFile();

            var context = CreateContext();
            
            foreach (var offenseEntity in offenses)
            {
                context.Offenses.Add(offenseEntity);

                context.SaveChanges();
            }
        }

        public List<OffenseEntity> GetEntitiesFromFile()
        {
            var fs = new FileStream(@"c:\Repos\1.csv", FileMode.Open);
            List<OffenseEntity> offenses;

            using (var sr = new StreamReader(fs))
            {
                var records = GetRecordsFromFile(sr);
                offenses = CreateOffenseList(records);
            }

            return offenses;
        }

        private static IEnumerable<string[]> GetRecordsFromFile(StreamReader sr)
        {
            var records = new List<string[]>();

            while (!sr.EndOfStream)
            {
                var data = sr.ReadLine()?.Split(';').ToArray();

                records.Add(data);
            }

            return records;
        }

        private static List<OffenseEntity> CreateOffenseList(IEnumerable<string[]> records)
        {
            var offenses = new List<OffenseEntity>();

            var orderedRecords = records.Select(r => r).GroupBy(r => new { person = r[2], date = r[5] });

            foreach (var record in orderedRecords)
            {
                var items = record.Select(r => r).ToList();
                var item = items.FirstOrDefault();

                var offense = new OffenseEntity
                {
                    Created = DateTime.Now,
                    Updated = DateTime.Now,
                    NumberOffense = item?[4] ?? "0",
                    IsDeleted = item?[15].Equals("True") ?? false,
                    IssuedDate = DateTime.Parse(item[5]),
                    PersonIssued = item[2],
                    OffenseItems = new List<OffenseItemEntity>(),
                };

                offense.OffenseItems.AddRange(items.Select(i =>
                {
                    var itm = i;

                    return new OffenseItemEntity
                    {
                        Status = i[13].Equals("True"),
                        Action = CheckForNull(i[9]),
                        ActualDate = DateTime.Parse(i[12]),
                        RequiredDate = DateTime.Parse(i[11]),
                        Comment = string.Join(';', CheckForNull(i[14]).Split('#')),
                        Created = DateTime.Now,
                        Heading = CheckForNull(i[6]),
                        IsDeleted = false,
                        Link = string.Join(';', CheckForNull(i[8]).Split('#')),
                        Number = CheckForNull(i[4]),
                        Object = CheckForNull(i[1]),
                        Prescription = string.Join(';', CheckForNull(i[7]).Split('#')),
                        ResponsiblePerson = CheckForNull(i[10]),
                        Updated = DateTime.Now,
                    };
                }));

                offenses.Add(offense);
            }

            return offenses;
        }

        private static string CheckForNull(string input)
        {
            if (string.IsNullOrEmpty(input) || input.Equals("null"))
            {
                return "";
            }

            return input;
        }

        private static MainContext CreateContext()
        {
            const string cs = "";

            var optionsBuilder = new DbContextOptionsBuilder<MainContext>();
            optionsBuilder.UseNpgsql(cs);

            return new MainContext(optionsBuilder.Options);
        }
    }
}
// 0  Id;
// 1  Object;
// 2  PersonIssued;
// 3  Brigade;
// 4  Number;
// 5  Date;
// 6  Heading;
// 7  Prescription;
// 8  Link;
// 9  Action;
// 10 ResponsiblePerson;
// 11 RequiredDate;
// 12 ActualDate;
// 13 Status;
// 14 Comment;
// 15 IsDeleted;
// 16 NumberInString
